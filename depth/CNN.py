
import chainer
from chainer import functions as F
from chainer import links as L

class CNN(chainer.Chain):
  def __init__(self):
    super(CNN,self).__init__(
      conv1=L.Convolution2D(4,8,5,pad=2),
      conv2=L.Convolution2D(8,16,5,pad=2),
      conv3=L.Convolution2D(16,32,5,pad=2),
      conv4=L.Convolution2D(32,16,5,pad=2),
      conv5=L.Convolution2D(16,8,5,pad=2),
      conv6=L.Convolution2D(8,1,5,pad=2)
    )

  def __call__(self,x,t):
    h,t = chainer.Variable(x),chainer.Variable(t)
    h = F.relu(self.conv1(h))
    h = F.relu(self.conv2(h))
    h = F.relu(self.conv3(h))
    h = F.relu(self.conv4(h))
    h = F.relu(self.conv5(h))
    y = F.sigmoid(self.conv6(h))
    return F.mean_squared_error(y, t)

  def forward(self,x):
    h = chainer.Variable(x)
    h = F.relu(self.conv1(h))
    h = F.relu(self.conv2(h))
    h = F.relu(self.conv3(h))
    h = F.relu(self.conv4(h))
    h = F.relu(self.conv5(h))
    return F.sigmoid(self.conv6(h))
