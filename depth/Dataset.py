import os
import numpy as np
try:
    from PIL import Image
    available = True
except ImportError as e:
    available = False
    _import_error = e
import six
import cv2
import random
import chamfer_dist
import input
from chainer.dataset import dataset_mixin
from chainer.datasets import tuple_dataset

class Dataset(dataset_mixin.DatasetMixin):

  def __init__(self, path, size, train=True, preprocess=True, dtype=np.float32):
    files = input.all_files(path)
    self.rgb_files = files[0]
    self.depth_files = files[1]
    self.train = train
    self.size = size
    self.dtype = dtype
    self.preprocess = preprocess

  def __len__(self):
    return len(self.rgb_files)

  def get_example(self, i):
    train = np.ndarray([4,self.size,self.size])
    rgb = np.array(Image.open(self.rgb_files[i]).resize((self.size,self.size)))
    depth = np.array(Image.open(self.depth_files[i]).resize((self.size,self.size)))
    if np.random.randint(2) and self.train:
      rgb = rgb[:,::-1,:]
      depth = depth[:,::-1]
    depth = depth/255.
    in_depth = np.zeros(depth.shape)
    gftt = cv2.xfeatures2d.SIFT_create()
    kp_list = gftt.detect(rgb)
    for key in kp_list:
      in_depth[int(key.pt[0])][int(key.pt[1])] = depth[int(key.pt[0])][int(key.pt[1])]
    if self.preprocess:
      in_depth = chamfer_dist.make_dist_map(in_depth)
      #in_depth = gauss.preprocess(in_depth)
    rgb = rgb/255.
    rgb=rgb.transpose(2,0,1)
    if self.train:
      for i in range(3):
        rgb_filter = random.uniform(0.8,1.2)
        rgb[i] = rgb[i] * rgb_filter
      rgb = rgb/rgb.max()
    train[:3]=rgb
    train[3]=in_depth
    truth = depth
    train = np.asarray(train,dtype=self.dtype)
    truth = np.asarray(truth,dtype=self.dtype)
    truth = truth.reshape(1,self.size,self.size)
    return train, truth
