import chainer
from chainer import functions as F
from chainer import links as L

class auto_encoder(chainer.Chain):
  def __init__(self):
    super(auto_encoder,self).__init__(
      en_conv1=L.Convolution2D(5,16,5),
      en_conv2=L.Convolution2D(16,32,5),
      en_conv3=L.Convolution2D(32,16,5),
      en_conv4=L.Convolution2D(16,1,5),
      de_conv4=L.Deconvolution2D(1,16,5,outsize=(88,88)),
      de_conv3=L.Deconvolution2D(16,32,5,outsize=(92,92)),
      de_conv2=L.Deconvolution2D(32,16,5,outsize=(96,96)),
      de_conv1=L.Deconvolution2D(16,1,5,outsize=(100,100))
    )

  def __call__(self,x,t):
    h,t = chainer.Variable(x),chainer.Variable(t)
    h = F.relu(self.en_conv1(h))
    h = F.relu(self.en_conv2(h))
    h = F.relu(self.en_conv3(h))
    h = F.relu(self.en_conv4(h))
    h = F.relu(self.de_conv4(h))
    h = F.relu(self.de_conv3(h))
    h = F.relu(self.de_conv2(h))
    y = F.sigmoid(self.de_conv1(h))
    return F.mean_squared_error(y, t)

  def forward(self,x):
    h = chainer.Variable(x)
    h = F.relu(self.en_conv1(h))
    h = F.relu(self.en_conv2(h))
    h = F.relu(self.en_conv3(h))
    h = F.relu(self.en_conv4(h))
    h = F.relu(self.de_conv4(h))
    h = F.relu(self.de_conv3(h))
    h = F.relu(self.de_conv2(h))
    y = F.sigmoid(self.de_conv1(h))
    return y
