#include <iostream>
#include <sys/types.h>
#include <dirent.h>
#include <queue>
#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/nonfree.hpp>

void bfs(cv::Mat &dist_map,cv::Mat &depth_map,int y,int x,cv::Mat space_d){
  int dx[] = {-1,0,1,0};
  int dy[] = {0,-1,0,1};
  int dist = 0;
  dist_map.at<int>(0,0) = dist;
  std::queue<cv::Point> q;
  cv::Point p(x,y);
  q.push(p);
  std::cout <<  dist_map.rows << " " <<  dist_map.cols <<std::endl;
  while(!q.empty()){
    cv::Point now = q.front();
    q.pop();
    dist++;
    for(int i=0;i<4;i++){
      int iy = now.y + dy[i];
      int ix = now.x + dx[i];
      if(iy < dist_map.rows && iy >= 0 && ix < dist_map.cols && ix >= 0){
        if(dist_map.at<int>(iy,ix) > dist){
          cv::Point p(ix,iy);
          q.push(p);
          dist_map.at<int>(iy,ix) = dist;
          if(space_d.at<int>(y,x) - dist >= 0)
            depth_map.at<int>(iy,ix) = space_d.at<int>(y,x) - dist;
          else
            depth_map.at<int>(iy,ix) = 0;
        }
      }
    }
  }
}

cv::Mat sift(cv::Mat depth,cv::Mat rgb){
  cv::Mat space_d(depth.rows,depth.cols,CV_8UC1,cv::Scalar(0));
  cv::initModule_nonfree();
  cv::Ptr<cv::FeatureDetector> detector = cv::FeatureDetector::create("SIFT");
  std::vector<cv::KeyPoint> keypoint;
  detector->detect(rgb, keypoint);
  std::cout << keypoint.size() << std:: endl;
  for(auto var:keypoint){
    space_d.at<int>((int)var.pt.y,(int)var.pt.x) = depth.at<int>((int)var.pt.y,(int)var.pt.x);
  }
  return space_d;
}

int main(int argc,char *argv[]){
  DIR* dir = opendir(argv[1]);
  int file_num = 0;
  if(dir!=NULL){
    struct dirent* dent = readdir(dir);
    while(dent != NULL){
      std::string file_name(dent->d_name);
      if(file_name == "." || file_name == ".."){
        dent = readdir(dir);
        continue;
      }
      dent = readdir(dir);
      file_num++;
    }
    closedir(dir);
    file_num /= 3;
    std::cout << "file num:" << file_num << std::endl;
    for(int i=0;i<file_num;i++){
      std::string path(argv[1]);
      std::string file_name = std::to_string(i);
      std::string rgb_path = path + "/" + file_name + ".ppm";
      std::string depth_path = path + "/" + file_name + ".pgm";
      cv::Mat depth = cv::imread(depth_path);
      cv::Mat rgb = cv::imread(rgb_path);
      cv::Mat space_d = sift(depth,rgb);
      cv::Mat dist_map(space_d.rows,space_d.cols,CV_8UC1,cv::Scalar(1));
      cv::Mat depth_map(space_d.rows,space_d.cols,CV_8UC1,cv::Scalar(1));
      for(int ix=0;ix < space_d.cols;ix++){
        for(int iy=0;iy < space_d.rows;iy++){
          if(space_d.at<int>(iy,ix) > 0){
            bfs(dist_map,depth_map,iy,ix,space_d);
          }
        }
      }
      std::string out_file = "dense" + std::string(argv[1]);
      //cv::imwrite(out_file,depth_map);
      std::cout << path + "/" + file_name << " done." << std::endl;
    }
  }
  return 0;
}
