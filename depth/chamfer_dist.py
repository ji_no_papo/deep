import numpy as np

def bfs(dist_map,depth_map,y,x,space_d):
    dx = [-1,0,1,0]
    dy = [0,-1,0,1]
    dist = 0
    dist_map[y][x] = dist
    depth_map[y][x] = space_d[y][x]
    queue = [(y,x)]
    while len(queue) != 0:
        now = queue[0]
        queue.pop(0)
        dist += 1
        for i in range(4):
            iy = now[0] + dy[i]
            ix = now[1] + dx[i]
            if iy < dist_map.shape[0] and iy >= 0 and ix < dist_map.shape[1] and ix >= 0:
                if dist_map[iy][ix] > dist:
                    queue.append((iy,ix))
                    dist_map[iy][ix] = dist
                    depth = space_d[y][x]
                    if depth >= 0:
                        depth_map[iy][ix] = depth
    return dist_map,depth_map

def make_dist_map(depth,kp_list):
    in_depth = np.zeros(depth.shape)
    for key in kp_list:
      in_depth[int(key.pt[0])][int(key.pt[1])] = depth[int(key.pt[0])][int(key.pt[1])]
    dist_map = np.ones([in_depth.shape[0],in_depth.shape[1]])*10000
    depth_map = np.zeros([in_depth.shape[0],in_depth.shape[1]])
    for iy in range(in_depth.shape[0]):
        for ix in range(in_depth.shape[1]):
            if in_depth[iy][ix] != 0:
                dist_map,depth_map = bfs(dist_map,depth_map,iy,ix,in_depth)
    return depth_map

if __name__ == '__main__':
  import input
  import cv2
  from PIL import Image
  from skimage import io
  size = 128
  testdataset = input.all_files('testdata')
  sum_loss = 0.
  sum_rms = 0.
  for j in range(len(testdataset[0])):
    files = [(testdataset[0][j],testdataset[1][j])]
    rgb = np.array(Image.open(testdataset[0][j]).resize((size,size)))
    depth = np.array(Image.open(testdataset[1][j]).resize((size,size)))
    gftt = cv2.xfeatures2d.SIFT_create()
    kp_list = gftt.detect(rgb)
    in_depth = make_dist_map(depth,kp_list)/255.
    depth = depth/255.
    io.imsave('test/test'+str(j)+'.png',(in_depth))
    diff = in_depth - depth
    diff = diff.ravel()
    sum_rms += np.array(diff.dot(diff) / diff.size)
  print('rms_loss:',np.sqrt(255.*(sum_rms/len(testdataset[0]))))
