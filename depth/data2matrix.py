import numpy as np
import chamfer_dist
import super_pixel
import gauss
from PIL import Image
from skimage import feature
from scipy import ndimage
import cv2
from multiprocessing import Pool
import multiprocessing
import random
import math

def select_window(data,size):
  shape = data.shape
  wx = np.random.rand()*(shape[1]-size)
  wy = np.random.rand()*(shape[0]-size)
  return (wx,wy)

def reshape(mat,shape):
  out = np.zeros(shape)
  y_size = int(mat.shape[0]/shape[0])
  x_size = int(mat.shape[1]/shape[1])
  for y in range(shape[0]):
    for x in range(shape[1]):
      i = mat[y*y_size:(y+1)*y_size,x*x_size:(x+1)*x_size]
      non_zero = i.nonzero()
      mean = 0
      if len(non_zero[0]) > 0:
        mean = np.mean(i[non_zero])
      out[y][x] = mean
  return out

def preprocess(var):
  data,size,ch,train_flag = var
  train = np.ndarray([ch,size,size])
  truth = np.ndarray([1,size,size])
  rgb = np.array(Image.open(data[0]).resize((size,size)))
  depth = np.array(Image.open(data[1]).resize((size,size)))
  #scale = random.uniform(1.0,1.5)
  scale = 1
  #train = np.ndarray([ch,rgb.shape[0],rgb.shape[1]])
  #truth = np.ndarray([1,rgb.shape[0],rgb.shape[1]])
  if np.random.randint(2) and train_flag:
    rgb = rgb[:,::-1,:]
    depth = depth[:,::-1]
  if ch ==  3:
    rgb = rgb/255.
    rgb = rgb.transpose(2,0,1)
    train[:3]=rgb
    if train_flag:
      rgb_filter = random.uniform(0.8,1.0)
      rgb = rgb * rgb_filter
  elif ch == 4:
    in_depth = np.zeros((size,size))
    gftt = cv2.xfeatures2d.SIFT_create()
    kp_list = gftt.detect(rgb)
    #in_depth = chamfer_dist.make_dist_map(depth,kp_list)
    #in_depth = chamfer_dist.make_dist_map(depth)
    #in_depth = super_pixel.make_near_depth(rgb,depth)
    in_depth = super_pixel.make_near_depth(rgb,depth,kp_list)
    if(in_depth.min() == in_depth.max()):
      in_depth = in_depth-in_depth.min()
    else:
      in_depth = (in_depth-in_depth.min())/(in_depth.max()-in_depth.min())
    rgb = rgb/255.
    rgb=rgb.transpose(2,0,1)
    if train_flag:
      rgb_filter = random.uniform(0.8,1.0)
      rgb = rgb * rgb_filter
      #rgb = rgb/rgb.max()
      rgb = rgb*scale
      in_depth = in_depth*scale
    train[:3]=rgb
    train[3]=in_depth
  depth = (depth-depth.min())/(depth.max()-depth.min())
  truth = depth
  return (train,truth)

def train_data(datas,size,xp,train_flag,ch=4):
  batch_size = len(datas)
  core = multiprocessing.cpu_count()
  if batch_size < core:
    p = Pool(batch_size)
  else:
    p = Pool(core)
  chs = [ch for i in range(batch_size)]
  sizes = [size for i in range(batch_size)]
  train_flag = [train_flag for i in range(batch_size)]
  pre_data = p.map(preprocess,zip(datas,sizes,chs,train_flag))
  p.terminate()
  train = np.array([i[0] for i in pre_data])
  truth = np.array([i[1] for i in pre_data])
  train = train.astype(np.float32)
  train = xp.asarray(train.reshape(batch_size,ch,size,size))
  truth = truth.astype(np.float32)
  truth = xp.asarray(truth.reshape(batch_size,1,size,size))
  return (train,truth)

def load(var):
  files,size,ch = var
  rgb = np.array(Image.open(files[0]).resize((size,size)))
  depth = np.array(Image.open(files[1]).resize((size,size)))
  in_depth = np.array(Image.open(files[2]).resize((size,size)))
  train = np.ndarray([ch,size,size])
  truth = np.ndarray([1,size,size])
  if np.random.randint(2):
    
    rgb = rgb[:,::-1,:]
    depth = depth[:,::-1]
    in_depth = in_depth[:,::-1]
  if(in_depth.min() == in_depth.max()):
    in_pdepth = in_depth-in_depth.min()
  else:
    in_depth = (in_depth-in_depth.min())/(in_depth.max()-in_depth.min())
  depth = (depth-depth.min())/(depth.max()-depth.min())
  rgb = rgb/255.
  rgb=rgb.transpose(2,0,1)
  for i in range(3):
    rgb_filter = random.uniform(0.8,1.2)
    rgb[i] = rgb[i] * rgb_filter
    rgb = rgb/rgb.max()
  scale = random.uniform(1.0,1.5)
  rgb = rgb*scale
  in_depth = in_depth*scale
  depth = depth*scale
  train[:3]=rgb
  train[3]=in_depth
  truth  = depth
  return (train,truth)

def img_load(files,size,xp,ch):
  batch_size = len(files)
  core = multiprocessing.cpu_count()
  if batch_size < core:
    p = Pool(batch_size)
  else:
    p = Pool(core)
  sizes = [size for i in range(batch_size)]
  chs = [ch for i in range(batch_size)]
  pre_data = p.map(load,zip(files,sizes,chs))
  p.terminate()
  train = np.array([i[0] for i in pre_data])
  truth = np.array([i[1] for i in pre_data])
  train = train.astype(np.float32)
  train = xp.asarray(train.reshape(batch_size,ch,size,size))
  truth = truth.astype(np.float32)
  truth = xp.asarray(truth.reshape(batch_size,1,size,size))
  return (train,truth)

if __name__ == "__main__":
  import input
  from skimage import io
  from tqdm import tqdm
  import data2matrix as d2m
  dataset = input.all_files('data')
  core = multiprocessing.cpu_count()
  for i in tqdm(range(int(len(dataset[0])/core)+1)):
    files = list(zip(dataset[0][i*core:i*core+core],dataset[1][i*core:i*core+core]))
    train_i,train_o = d2m.train_data(files,128,np,False,4)
    for j in range(core):
      img = train_i[j][3]
      img = (img-img.min())/(img.max()-img.min())
      io.imsave("inputdata/" + str(i*core+j) + ".png",img)

