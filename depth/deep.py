import tensorflow as tf
import input
import data2matrix as d2m

isize = 100
osize = 20

def weight_variable(shape):
    initial = tf.truncated_normal(shape,stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x,w):
    return tf.nn.conv2d(x,w,strides=[1,1,1,1],padding='SAME')


sess = tf.InteractiveSession()
with tf.name_scope('input') as scope:
  x = tf.placeholder("float",[10,4*isize*isize],name='input_unit')

with tf.name_scope('hidden1') as scope:
  w_conv1 = tf.Variable(tf.random_normal([5,5,4,32],stddev=0.1),name='w_conv1')
  b_conv1 = tf.Variable(tf.random_normal([32],stddev=0.1))
  x_img = tf.reshape(x, [10,isize,isize,4])
  h_conv1 = tf.nn.relu(conv2d(x_img, w_conv1)*b_conv1)

w_hist = tf.histogram_summary("w_conv1", w_conv1)
with tf.name_scope('hidden2') as scope:
  w_conv2 = tf.Variable(tf.random_normal([5,5,32,64],stddev=0.1))
  b_conv2 = tf.Variable(tf.random_normal([64],stddev=0.1))
  h_conv2 = tf.nn.relu(conv2d(h_conv1, w_conv2)+b_conv2)

with tf.name_scope('hidden3') as scope:
  w_conv3 = tf.Variable(tf.random_normal([5,5,64,32],stddev=0.1))
  b_conv3 = tf.Variable(tf.random_normal([32],stddev=0.1))
  h_conv3 = tf.nn.relu(conv2d(h_conv2, w_conv3)+b_conv3)

with tf.name_scope('hiden4') as scope:
  w_conv4 = tf.Variable(tf.random_normal([5,5,32,1],stddev=0.1))
  h_conv4 = tf.nn.relu(conv2d(h_conv3, w_conv4))
  h_conv4 = tf.reshape(h_conv4,[10,10000])

with tf.name_scope('output') as scope:
  w = tf.Variable(tf.random_normal([10000, osize*osize],stddev=0.1))
  b = tf.Variable(tf.random_normal([osize*osize],stddev=0.1))
  y_ = tf.nn.relu(tf.matmul(h_conv4, w)+b)
  y_ = tf.reshape(y_,[10,osize*osize])
  y = tf.placeholder("float",[10,osize*osize])

l2_loss = tf.nn.l2_loss(y - y_)
l2_loss = tf.reduce_mean(l2_loss)
loss_scalar = tf.scalar_summary("loss", l2_loss)
train_step = tf.train.AdagradOptimizer(0.001).minimize(l2_loss)
merged = tf.merge_all_summaries()
sess.run(tf.initialize_all_variables())
saver = tf.train.Saver()
board = tf.train.SummaryWriter('./logs/test', graph_def=sess.graph_def)

for i in range(100000):
  files = input.select_files()
  train_i = d2m.traindatas(files,isize)
  train_o = d2m.truthdata(files,osize)
  sess.run(train_step,feed_dict={x:train_i,y:train_o})
  if not i % 100:
    loss = sess.run([l2_loss,merged],feed_dict={x:train_i,y:train_o})
    board.add_summary(loss[1],i)
    print ('l2_loss:%.2f' % (loss[0]))
save_path = saver.save(sess,'/home/ji-no/deep/depth/model/sess1.ckpt')
print 'saved:'+save_path
