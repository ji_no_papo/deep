import numpy as np
from PIL import Image
from PIL import ImageFilter
import input
import segment
from skimage.future import graph
from scipy import optimize
import cv2
import super_pixel
from tqdm import tqdm
import linear
from skimage.segmentation import slic
from skimage.segmentation import find_boundaries 
from skimage.future import graph
from skimage import io
import point2obj
import make_grid_tri
import os

def in_mat(coord,shape):
  return coord[0] >= 0 and coord[0] < shape[0] and coord[1] >= 0 and coord[1] < shape[1]

def depth_trust(cont,point,depth):
  coord = cont.dot(np.r_[point,1].T)
  x = int(coord[0]/coord[2])
  y = int(coord[1]/coord[2])
  if in_mat((y,x),depth.shape):
    return np.exp(-abs(depth[y][x]-coord[2]))
  else:
    return 1

def sfm_cnn(gt,depth,rgb,thread=5):
  seg = slic(rgb,sigma=1)
  g = graph.RAG(seg,connectivity=2)
  for n in g:
    g.node[n].update({
      "sfm_flag":False
    })
  seg_depth = np.zeros(depth.shape)
  for n in g:
    mean_depth = np.mean(depth[seg == n])
    if len(gt[(seg == n)*(gt > 0)])>0:
      mean_gt = np.mean(gt[(seg == n)*(gt > 0)])
    else:
      mean_gt = 0
    if abs(mean_depth-mean_gt) < thread:
      g.node[n]["sfm_flag"] = True
  g_near = g.copy()
  for n in g:
    if g.node[n]["sfm_flag"] == False:
      continue
    for neg in g.edge[n]:
      if n == neg:
        continue
      g_near.node[neg]["sfm_flag"] = True
  out = np.ones(depth.shape)*-1
  for n in g:
    if g.node[n]["sfm_flag"]: #or g_near.node[n]["sfm_flag"]:
      out[seg == n] = depth[seg == n]
  return out

def remove_noise(depth,cont,rgb,depths,conts,ind,seg_num,thread = 5):
  seg = slic(rgb,sigma=1,n_segments=seg_num)
  g = graph.RAG(seg,connectivity=2)
  for n in g:
    g.node[n].update({
      "coord_list":[],
      "mean_error":0
    })
  min_sample = 100000000000
  for n in g:
    g.node[n]["coord_list"] = np.where(seg == n)
  for n in g:
    if min_sample > len(g.node[n]["coord_list"]):
      min_sample = len(g.node[n]["coord_list"])
  min_sample = int(0.5*min_sample)
  for n in g:
    coord_list = g.node[n]["coord_list"]
    sample = np.random.choice(len(coord_list[0]),min_sample)
    points = [np.array([coord_list[0][i]*depth[coord_list[0][i]][coord_list[1][i]],coord_list[1][i]*depth[coord_list[0][i]][coord_list[1][i]],depth[coord_list[0][i]][coord_list[1][i]]]) for i in sample]
    points = [linear.affine(point,cont) for point in points]
    for point in points:
      count = 0
      error = 0
      if ind > 0:
        error += depth_trust(conts[ind-1],point,depths[ind-1])
        count += 1
      if ind+1 < len(depths):
        error += depth_trust(conts[ind+1],point,depths[ind+1])
        count += 1
      g.node[n]["mean_error"] += error/count
    g.node[n]["mean_error"] /= min_sample
  for n in g:
    if g.node[n]["mean_error"] <= np.exp(-thread):
      depth[g.node[n]["coord_list"]] = -1
  return depth

def sample_vertex(depth,rgb,seg_num,grid_size):
  sample_num = 1
  seg = slic(rgb,n_segments=seg_num,sigma=1)
  edge = find_boundaries(seg,mode='innner')
  out = np.zeros(depth.shape)
  for i in range(seg.max()):
    point = np.where(seg == i)
    mean_depth = np.mean(depth[point])
    y = int(np.mean(point[0]))
    x = int(np.mean(point[1]))
    if depth[y][x]< 0:
      continue
    out[y][x] = depth[y][x]
  for y in range(int(depth.shape[0]/grid_size)):
    for x in range(int(depth.shape[1]/grid_size)):
      point = np.where(edge[y*grid_size:(y+1)*grid_size,x*grid_size:(x+1)*grid_size] == 1)
      if len(point[0]) == 0:
        continue
      sample = np.random.choice(len(point[0]),min(sample_num,len(point[0])),replace=False)
      point = (point[0][sample]+y*grid_size,point[1][sample]+x*grid_size)
      out[point] = depth[point]
  out[out < 0] = 0
  return out

def patch2mat(cont,patchs,shape):
  depth = np.zeros(shape)
  trust = np.zeros(shape)
  patchs = np.array([(cont.dot(patch["locate"].T).reshape(3),len(patch["cameras"])) for patch in patchs])
  points = np.array([(patch[0]/patch[0][2]).astype(int) for patch in patchs])
  d = [patch[0][2] for patch in patchs]
  depth[(points.T[1],points.T[0])] = d
  trust[(points.T[1],points.T[0])] = patchs[:,1]
  return (depth,trust)

def patch2ply(patchs,rgb,cont,ply_name):
  locates = np.array([cont.dot(patch["locate"].T).reshape(3) for patch in patchs])
  locates = np.array([((locate[1]/locate[2]).astype(int),(locate[0]/locate[2]).astype(int)) for locate in locates])
  points = np.array([patch["locate"] for patch in patchs])
  f = open(ply_name,'w')
  f.write("ply\n")
  f.write("format ascii 1.0\n")
  f.write("element vertex "  + str(len(points)) + "\n")
  f.write("property float x\n")
  f.write("property float y\n")
  f.write("property float z\n")
  f.write("property uchar diffuse_red\n")
  f.write("property uchar diffuse_green\n")
  f.write("property uchar diffuse_blue\n")
  f.write("end_header\n")
  for i in range(len(points)):
    for j in range(3):
      f.write(str(points[i][j])+" ")
    for j in range(3):
      f.write(str(rgb[locates.T[0][i]][locates.T[1][i]][j])+" ")
    f.write("\n")
  f.close()


def write_ply(depth,cont,rgb,ply_name):
  rgb = [rgb[y][x]
           for y in range(0,rgb.shape[0],10)
           for x in range(0,rgb.shape[1],10)
           if depth[y][x] >= 0]
  normal = linear.make_normal(depth)
  point = linear.depth2point(depth,cont)
  f = open(ply_name,'w')
  f.write("ply\n")
  f.write("format ascii 1.0\n")
  f.write("element vertex "  + str(len(point)) + "\n")
  f.write("property float x\n")
  f.write("property float y\n")
  f.write("property float z\n")
  f.write("property float nx\n")
  f.write("property float ny\n")
  f.write("property float nz\n")
  f.write("property uchar diffuse_red\n")
  f.write("property uchar diffuse_green\n")
  f.write("property uchar diffuse_blue\n")
  f.write("end_header\n")
  for i in range(len(point)):
    for j in range(3):
      f.write(str(point[i][j])+" ")
    for j in range(3):
      f.write(str(normal[i][j])+" ")
    for j in range(3):
      f.write(str(rgb[i][j])+" ")
    f.write("\n")
  f.close()

def make_ply(rgb_dir_path,depth_dir_path,cont_dir_path,patch_path,out_dir='.',seg_num=50,grid_size=50):
  rgb_list = input.find_all_files(rgb_dir_path)
  depth_list = input.find_all_files(depth_dir_path)
  cont_list = input.find_all_files(cont_dir_path)
  rgbs = [np.array(cv2.imread(path))[::,::,::-1] for path in rgb_list]
  depths = [np.array(cv2.imread(path,cv2.IMREAD_GRAYSCALE)) for path in depth_list]
  depths = [cv2.resize(depth,tuple(reversed(rgb.shape[:2])),interpolation=cv2.INTER_NEAREST)
            for (depth,rgb) in zip(depths,rgbs)]
  #rgbs = [np.rot90(rgb) for rgb in rgbs]
  #depths = [np.rot90(depth) for depth in depths]
  patch= input.load_patch(patch_path)
  conts = [input.load_cont(path) for path in cont_list]
  print("patch2mat")
  tmp = [patch2mat(conts[int(key)],patch[key],rgbs[int(key)].shape[:2]) for key in tqdm(sorted(patch.keys()))]
  rgbs = [rgbs[int(key)] for key in tqdm(sorted(patch.keys(),key=input.cmp_to_key(input.cmp)))]
  depths = [depths[int(key)] for key in tqdm(sorted(patch.keys(),key=input.cmp_to_key(input.cmp)))]
  conts = [conts[int(key)] for key in tqdm(sorted(patch.keys(),key=input.cmp_to_key(input.cmp)))]
  rgb_list = [rgb_list[int(key)] for key in tqdm(sorted(patch.keys(),key=input.cmp_to_key(input.cmp)))]
  patch = [patch[key] for key in tqdm(sorted(patch.keys(),key=input.cmp_to_key(input.cmp)))]
  gts = [var[0] for var in tmp]
  print("adjast_depths")
  depths = [linear.adjast(depth,gt) for (depth,gt) in tqdm(zip(depths,gts))]
  #[write_ply(depth,cont,rgb,out_dir+'/pure'+str(i)+'.ply') for i,(depth,cont,rgb) in enumerate(tqdm(zip(depths,conts,rgbs)))]
  vertexs = [sample_vertex(depth,rgb,seg_num,grid_size) for (depth,rgb) in tqdm(zip(depths,rgbs))]
  [point2obj.point2obj(vertex,rgb,cont,path,out_dir+'/pure'+str(i)) for i,(vertex,cont,rgb,path) in enumerate(tqdm(zip(vertexs,conts,rgbs,rgb_list)))]
  [make_grid_tri.make_obj(depth,rgb,cont,path,grid_size,out_dir+'/pure_grid'+str(i)) for i,(depth,cont,rgb,path) in enumerate(tqdm(zip(depths,conts,rgbs,rgb_list)))]
  [patch2ply(p,rgb,cont,out_dir+'/sfm_spmple'+str(i)+'.ply') for i,(p,rgb,cont) in enumerate(tqdm(zip(patch,rgbs,conts)))]
  trusts = [var[1] for var in tmp]
  #print("cnn_model")
  #out = [sfm_cnn(gt,depth,rgb) for (gt,depth,rgb) in tqdm(zip(gts,depths,rgbs))]
  #[write_ply(depth,cont,rgb,out_dir+'/sfm'+str(i)+'.ply') for i,(depth,cont,rgb) in enumerate(tqdm(zip(out,conts,rgbs)))]
  #vertexs = [sample_vertex(depth,rgb) for (depth,rgb) in tqdm(zip(out,rgbs))]
  #[point2obj.point2obj(vertex,rgb,cont,path,out_dir+'/sfm'+str(i)) for i,(vertex,cont,rgb,path) in enumerate(tqdm(zip(vertexs,conts,rgbs,rgb_list)))]
  print("remove noise")
  no_noise_depths = [remove_noise(depth,cont,rgb,depths,conts,i,seg_num) for i,(depth,cont,rgb) in enumerate(tqdm(zip(depths,conts,rgbs)))]
  #vertexs = [sample_vertex(depth,rgb) for (depth,rgb) in tqdm(zip(no_noise_depths,rgbs))]
  #[point2obj.point2obj(vertex,rgb,cont,path,out_dir+'/no_noise'+str(i)) for i,(vertex,cont,rgb,path) in enumerate(tqdm(zip(vertexs,conts,rgbs,rgb_list)))]
  [write_ply(depth,cont,rgb,out_dir+'/no_noise'+str(i)+'.ply') for i,(depth,cont,rgb) in enumerate(tqdm(zip(no_noise_depths,conts,rgbs)))]
  print("trust comp")
  trusts = [linear.reshape(trust,(100,100)) for trust in trusts]
  rgbs_min = [cv2.resize(rgb,(100,100),interpolation=cv2.INTER_AREA) for rgb in rgbs]
  trusts = [super_pixel.fill_spx(rgb,trust) for (rgb,trust) in tqdm(zip(rgbs_min,trusts))]
  trusts = [cv2.resize(trust,tuple(reversed(rgb.shape[:2])),interpolation=cv2.INTER_LINEAR)
            for (trust,rgb) in zip(trusts,rgbs)]
  print("make pointcloud")
  tpoints = [linear.depth2point(depth,cont,trust) for (depth,cont,trust) in tqdm(zip(depths,conts,trusts))]
  print("smooth depth")
  depths = [cv2.resize(depth,(100,100),interpolation=cv2.INTER_AREA)
            for depth in depths]
  no_noise_depths = [cv2.resize(no_noise_depth,(100,100),interpolation=cv2.INTER_AREA)
            for no_noise_depth in no_noise_depths]
  gts = [linear.reshape(gt,(100,100)) for gt in gts]

  div = np.eye(100)
  div[-1:,-1:] = 0
  div -= np.c_[np.zeros((100,1)),div[:,:-1]]
  divx = np.zeros((10000,10000))
  divy = np.zeros((10000,10000))
  for i in range(100):
    divx[i*100:i*100+100,i*100:i*100+100] = div
  #divx = divx.dot(divx)
  div = np.eye(100*99)
  divy[:div.shape[0],:div.shape[0]] += div
  divy[:div.shape[0],100:100+div.shape[0]] -= div
  #divy = divy.dot(divy)

  depths = [linear.solve(depth,gt,trust,cont,tpoints,no_noise_depth,rgb.shape,divx,divy,
                  unary_key_cost=0.05,unary_other_cost=0.05,smooth_cost=1,multi_cost=0)
           for (trust,depth,gt,cont,rgb,no_noise_depth) in tqdm(zip(trusts,depths,gts,conts,rgbs,no_noise_depths))]
  depths = [depth.reshape(100,100) for depth in depths]
  no_noise_depths = [no_noise_depth.reshape(100,100) for no_noise_depth in no_noise_depths]
  depths = [cv2.resize(depth,tuple(reversed(rgb.shape[:2])),interpolation=cv2.INTER_NEAREST)
            for (depth,rgb) in zip(depths,rgbs)]
  no_noise_depths = [cv2.resize(no_noise_depth,tuple(reversed(rgb.shape[:2])),interpolation=cv2.INTER_NEAREST)
            for (no_noise_depth,rgb) in zip(no_noise_depths,rgbs)]
  #[io.imsave(out_dir+'/smooth'+str(i)+'.png',(depth - depth.min())/(depth.max() - depth.min())) for i,depth in enumerate(depths)]
  print("point2depth")
  #[write_ply(depth,cont,rgb,out_dir+'/'+str(i)+'.ply') for i,(depth,cont,rgb) in enumerate(tqdm(zip(depths,conts,rgbs)))]
  vertexs = [sample_vertex(depth,rgb,seg_num,grid_size) for (depth,rgb) in tqdm(zip(depths,rgbs))]
  [point2obj.point2obj(vertex,rgb,cont,path,out_dir+'/'+str(i)) for i,(vertex,cont,rgb,path) in enumerate(tqdm(zip(vertexs,conts,rgbs,rgb_list)))]
  [make_grid_tri.make_obj(depth,rgb,cont,path,grid_size,out_dir+'/grid'+str(i)) for i,(depth,cont,rgb,path) in enumerate(tqdm(zip(depths,conts,rgbs,rgb_list)))]
  [io.imsave(out_dir+'/'+os.path.split(path)[1],rgb/255.) for rgb,path in zip(rgbs,rgb_list)]

if __name__ == '__main__':
  from skimage import io
  import cv2
  rgb = np.array(Image.open('example/danbo/rgb/input000.png'))
  img = np.array(cv2.imread('test.png', cv2.IMREAD_GRAYSCALE))
  depth = np.array(Image.open('super_edge/test0000.png').resize(rgb.shape[:2][::-1]))
  out = sample_vertex(depth,rgb)
  io.imsave('test.png',(out/out.max()))
