import numpy as np
from PIL import Image
from skimage import feature
from scipy import ndimage
from skimage import io


def mask(rgb_path):
  edge = np.array(Image.open(rgb_path).convert('L'), 'f')
  edge = feature.canny(edge,8)*255
  edge = ndimage.gaussian_filter(edge,sigma=2)
  edge = edge/edge
  for i in range(4):
    mask = np.array([np.random.randint(2) for i in xrange(edge.shape[0]*edge.shape[1])]).reshape(edge.shape[0],edge.shape[1])
    edge = edge*mask
  return edge


edge = mask('./data/test/215.ppm')
io.imsave('mask.png',(edge*255))
