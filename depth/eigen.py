import numpy as np
import chainer
from chainer import cuda, optimizers
import data2matrix as d2m
import input
import pickle
from skimage import io
import sys
import networks
import VGGNet

def run(name):
  gpu_flag = 0

  if gpu_flag >= 0:
    cuda.check_cuda_available()

  isize = 128

  #VGG = VGGNet.VGGNet()
  #serializers.load_hdf5('model/VGG.model', VGG)
  model1 = networks.Net().networks['eigen_net']
  model2 = networks.Net().networks['eigen_final_net']
  xp = cuda.cupy if gpu_flag >= 0 else np
  if gpu_flag >= 0:
    cuda.get_device(gpu_flag).use()
    #VGG.to_gpu()
    model1.to_gpu()
    model2.to_gpu()

  # setup optimizer
  #optimizer = optimizers.AdaGrad()
  optimizer = optimizers.Adam()
  #optimizer = optimizers.RMSprop()
  optimizer.setup(model2)

  batchsize = 8

  dataset = input.all_files('data')
  print("dataset:" + str(len(dataset[0])))
  now_dataset = [[],[],[]]
  for i in range(3):
    now_dataset[i] = dataset[i][:]
  testdataset = input.all_files('testdata')
  epoch = 0

  while True:

    if epoch%100000 == 0:
      sum_loss = 0.
      sum_rms = 0.
      for j in range(len(testdataset[0])):
        files = [(testdataset[0][j],testdataset[1][j],testdataset[2][j])]
        test_i,test_o = d2m.train_data(files,isize,xp,model2.ich)
        io.imsave('test/truth'+str(j)+'.png',chainer.cuda.to_cpu(test_o)[0][0])
        model1.train = False
        in_depth = model1(test_i,xp.zeros(test_i.shape))
        model1.train = True
        for i in range(len(test_i)):
          test_i[i][3] = in_depth.data[i][0]
        model2.train = False
        out = model2(test_i,xp.zeros(test_i.shape))
        model2.train = True
        loss = model2(test_i,test_o)
        out.to_cpu()
        io.imsave('test/test'+str(j)+'.png',out.data[0][0])
        sum_loss = sum_loss + loss.data
        diff = out.data - chainer.cuda.to_cpu(test_o)
        diff = diff.ravel()
        sum_rms += np.array(diff.dot(diff) / diff.size)
      print('loss_functions_loss:',(sum_loss/len(testdataset[0])))
      print('rms_loss:',np.sqrt(255.*(sum_rms/len(testdataset[0]))))
      pickle.dump(model2,open('model/eigen_net.pkl','wb'),-1)
      pickle.dump(model2,open('model/eigen_final_net.pkl','wb'),-1)

    files = input.select_files(dataset,now_dataset,batchsize)
    train_i,train_o = d2m.train_data(files,isize,xp,model2.ich)
    optimizer.zero_grads()
    optimizer.update(model1,train_i,train_o)
    loss1 = model1(train_i,train_o)
    model1.train = False
    in_depth = model1(train_i,xp.zeros(test_i.shape))
    model1.train = True
    for i in range(len(train_i)):
      train_i[i][3] = in_depth.data[i][0]
    optimizer.zero_grads()
    optimizer.update(model2,train_i,train_o)
    loss2 = model2(train_i,train_o)
    epoch += batchsize
    sys.stdout.write("\r%d:%f:%f" % epoch,loss1.data,loss2.data)
    sys.stdout.flush()
    if epoch >= len(dataset[0]):
      epoch = 0 

if __name__ == '__main__':
  run('eigen_net')
