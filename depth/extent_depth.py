import numpy as np
from chainer import cuda
import sys
import input
import fnmatch
from PIL import Image
from skimage import io

gpu_flag = 0
isize = 100

model = input.load_model('model/depth_mask_5ch.pkl')
xp = cuda.cupy if gpu_flag >= 0 else np
#if gpu_flag >= 0:
#  print "This use GPU"
#  sys.exit()

rgb_files = []
depth_files = []
fill_files = []
files = []
for file in input.fild_all_files('danbo'):
  if fnmatch.fnmatch(file,'*depth*'):
    depth_files.append(file)
    fill_files.append(file)
  if fnmatch.fnmatch(file,'*input*'):
    rgb_files.append(file)
rgb_files.sort(cmp=lambda x,y: cmp(input.path2int(x), input.path2int(y)))
depth_files.sort(cmp=lambda x,y: cmp(input.path2int(x), input.path2int(y)))
fill_files.sort(cmp=lambda x,y: cmp(input.path2int(x), input.path2int(y)))
for i in range(len(rgb_files)):
  files.append((rgb_files[i],depth_files[i],fill_files[i]))

for ind,i in enumerate(files):
  rgb = np.array(Image.open(i[0]))
  depth = np.array(Image.open(i[2]))/255.
  out_data = np.zeros([1,1,rgb.shape[0],rgb.shape[1]])
  print(i)
  for j in range(40):
    for ix in range((rgb.shape[1]-isize)/isize):
      for iy in range((rgb.shape[0]-isize)/isize):
        if rgb.shape[1] < ix*(isize+(isize/4)*(j%4)) or rgb.shape[0] < iy*(isize+(isize/4)*(j%4)):
          continue
        in_data = np.ndarray([1,5,isize,isize])
        window = (iy*isize+(isize/4)*(j%4),ix*isize+(isize/4)*(j%4))
        mrgb = rgb[window[0]:window[0]+isize,window[1]:window[1]+isize]
        mdepth = depth[window[0]:window[0]+isize,window[1]:window[1]+isize]
        mrgb=mrgb.transpose(2,0,1)
        mrgb = mrgb/255.
        mask = np.ceil(mdepth)
        in_data[0][:3] = mrgb
        in_data[0][3] = mdepth
        in_data[0][4] = mask
        in_data = in_data.astype(np.float32)
        in_data = xp.asarray(in_data.reshape(1,5,isize,isize))
        if len(mdepth[mdepth > 0]) > 0:
          out = model.forward(in_data)
          out.to_cpu()
          for mask_count in range(10):
            mask = np.array([np.random.randint(2) for irange in xrange(isize*isize)]).reshape(isize,isize)
            out = out*mask
          depth[window[0]:window[0]+isize,window[1]:window[1]+isize] = out.data[0][0]
  io.imsave('test/danbo_test1_full'+str(ind)+'.png',(depth))
