import numpy as np
from math import exp
from PIL import Image
from skimage import io

def preprocess(space_d):
    kp_list = np.nonzero(space_d)
    dence_d = np.zeros([space_d.shape[0],space_d.shape[1]])
    G = np.array([[gauss(np.array((kp_list[0][i],kp_list[1][i])),np.array((kp_list[0][j],kp_list[1][j])))for j in range(len(kp_list[0]))]for i in range(len(kp_list[0]))])
    d = np.array( [space_d[kp_list[0][i]][kp_list[1][i]] for i in range(len(kp_list[0]))])
    a = 0.0001*np.ones((len(kp_list[0]),len(kp_list[0]))) + G.T*G
    b = G*np.matrix(d).T
    c = np.linalg.solve(G,np.matrix(d).T)
    c = np.array(c.T)[0]
    dence_d = np.array([[
        np.dot(c,np.array([gauss(np.array((kp_list[0][k],kp_list[1][k])),np.array([i,j])) for k in range(len(kp_list[0]))]))
        for j in range(space_d.shape[1])]
             for i in range(space_d.shape[0])])
    test_d = np.array([dence_d[kp_list[0][i]][kp_list[1][i]] for i in range(len(kp_list[0]))])
    if dence_d.min() < 0:
        dence_d = dence_d + abs(dence_d.min())
    return dence_d/dence_d.max()

def gauss(p_1,p_2):
    sigma = 7
    l2_norm = np.linalg.norm(p_1 - p_2)
    return exp(-1*(l2_norm**2)/(2*(sigma**2)))

if __name__ == '__main__':
    import cv2
    import time
    start = time.time()
    size = 100
    rgb = np.array(Image.open("data/100.ppm").resize((size,size)))
    depth = np.array(Image.open("data/100.pgm").resize((size,size)))/255.
    in_depth = np.zeros((rgb.shape[0],rgb.shape[1]))
    gray= cv2.cvtColor(rgb,cv2.COLOR_BGR2GRAY)
    gftt = cv2.FeatureDetector_create("SIFT")
    kp_list = gftt.detect(rgb)
    for key in kp_list:
        in_depth[key.pt[1]][key.pt[0]] = depth[key.pt[1]][key.pt[0]]
    dence_d =preprocess(in_depth)
    print(time.time() -start)
    io.imsave('gauss.png',dence_d)
