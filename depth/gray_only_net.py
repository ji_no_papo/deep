import chainer
from chainer import functions as F
from chainer import links as L
import variance_loss

class network(chainer.Chain):
  def __init__(self):
    super(network,self).__init__(
      en_conv1=L.Convolution2D(1,16,5,pad=2),
      en_conv2=L.Convolution2D(16,32,5,pad=2),
      en_conv3=L.Convolution2D(32,16,5,pad=2),
      en_conv4=L.Convolution2D(16,1,5,pad=2),
    )

  def __call__(self,x,t):
    hx,t = chainer.Variable(x),chainer.Variable(t)
    h = F.relu(self.en_conv1(hx))
    h = F.relu(self.en_conv2(h))
    h = F.relu(self.en_conv3(h))
    y = F.sigmoid(self.en_conv4(h))
    return F.mean_squared_error(y, t)

  def loss(self,x,t):
    hx,t = chainer.Variable(x),chainer.Variable(t)
    h = F.relu(self.en_conv1(hx))
    h = F.relu(self.en_conv2(h))
    h = F.relu(self.en_conv3(h))
    y = F.sigmoid(self.en_conv4(h))
    return F.mean_squared_error(y, t)


  def forward(self,x):
    h = chainer.Variable(x)
    h = F.relu(self.en_conv1(h))
    h = F.relu(self.en_conv2(h))
    h = F.relu(self.en_conv3(h))
    y = F.sigmoid(self.en_conv4(h))
    return y
