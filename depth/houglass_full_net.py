import chainer
from chainer import functions as F
from chainer import links as L
import variance_loss
import cupy as xp
import inception_module

class network(chainer.Chain):

  def __init__(self):
    self.ich = 4
    self.och = 1
    self.train = False
    super(network,self).__init__(
      conv1=L.Convolution2D(3,31,3,pad=1),
      inception1_1=inception_module.Inception(32,4,8,3,7,11),
      inception2_1=inception_module.Inception(32,8,4,3,5,7),
      inception2_2=inception_module.Inception(32,8,4,3,5,7),
      inception2_3=inception_module.Inception(32,8,4,3,5,7),
      inception2_4=inception_module.Inception(32,8,8,3,7,11),
      inception2_5=inception_module.Inception(32,8,4,3,5,7),
      inception2_6=inception_module.Inception(32,4,8,3,7,11),
      inception3_1=inception_module.Inception(32,8,4,3,5,7),
      inception3_2=inception_module.Inception(32,16,4,3,5,7),
      inception3_3=inception_module.Inception(64,16,4,3,5,7),
      inception3_4=inception_module.Inception(64,16,8,3,7,11),
      inception3_5=inception_module.Inception(64,16,4,3,5,7),
      inception3_6=inception_module.Inception(64,8,4,3,5,7),
      inception4_1=inception_module.Inception(64,16,4,3,5,7),
      inception4_2=inception_module.Inception(64,16,4,3,5,7),
      inception4_3=inception_module.Inception(64,16,4,3,5,7),
      inception4_4=inception_module.Inception(64,16,4,3,5,7),
      inception4_5=inception_module.Inception(64,16,4,3,5,7),
      inception4_6=inception_module.Inception(64,16,8,3,7,11),
      inception5_1=inception_module.Inception(64,16,4,3,5,7),
      inception5_2=inception_module.Inception(64,16,4,3,5,7),
      inception5_3=inception_module.Inception(64,16,4,3,5,7),
      conv2=L.Convolution2D(16,1,3,pad=1),
    )

  def __call__(self,x,t):
    if self.train:
      t = chainer.Variable(t)
    rgb,d = chainer.Variable(x[:,:3,:,:]),chainer.Variable(x[:,3,:,:].reshape(len(x),1,len(x[0][0]),len(x[0][0][0])))
    h = F.relu(self.conv1(rgb))
    h = F.concat((h,d),1)
    h1 = F.relu(self.inception1_1(h))

    h2 = F.relu(self.inception2_1(F.max_pooling_2d(h,2,stride=2)))
    h2 = F.relu(self.inception2_2(h2))

    h3 = F.relu(self.inception3_1(F.max_pooling_2d(h2,2,stride=2)))
    h3 = F.relu(self.inception3_2(h3))

    h4 = F.relu(self.inception4_1(F.max_pooling_2d(h3,2,stride=2)))
    h4 = F.relu(self.inception4_2(h4))

    h5 = F.relu(self.inception5_1(F.max_pooling_2d(h4,2,stride=2)))
    h5 = F.relu(self.inception5_2(h5))
    h5 = F.relu(self.inception5_3(h5))

    h4 = F.relu(self.inception4_3(h4))
    h4 = F.relu(self.inception4_4(h4))

    h4 = F.relu(self.inception4_5(h4+F.unpooling_2d(h5,2,stride=2,outsize=(16,16))))
    h4 = F.relu(self.inception4_6(h4))

    h3 = F.relu(self.inception3_3(h3))
    h3 = F.relu(self.inception3_4(h3))

    h3 = F.relu(self.inception3_5(h3+F.unpooling_2d(h4,2,stride=2,outsize=(32,32))))
    h3 = F.relu(self.inception3_6(h3))

    h2 = F.relu(self.inception2_3(h2))
    h2 = F.relu(self.inception2_4(h2))

    h2 = F.relu(self.inception2_5(h2+F.unpooling_2d(h3,2,stride=2,outsize=(64,64))))
    h2 = F.relu(self.inception2_6(h2))

    #y = F.relu(self.conv2(h1+F.unpooling_2d(h2,2,stride=2,outsize=(128,128))))
    y = F.sigmoid(self.conv2(h1+F.unpooling_2d(h2,2,stride=2,outsize=(128,128))))
    if self.train:
      #y.data = y.data/x.max()
      #t.data = t.data/x.max()
      #y = F.log(y)
      #t = F.log(t+0.0000001)
      divx_y = (y.data[:,:,1:,:] - y.data[:,:,:-1,:])#*divx_mask
      divx_t = (t.data[:,:,1:,:] - t.data[:,:,:-1,:])#*divx_mask
      divy_y = (y.data[:,:,:,1:] - y.data[:,:,:,:-1])#*divy_mask
      divy_t = (t.data[:,:,:,1:] - t.data[:,:,:,:-1])#*divy_mask
      #return F.mean_squared_error(y,t)
      return F.mean_squared_error(y,t)+F.mean_squared_error(divy_y,divy_t)+F.mean_squared_error(divx_y,divx_t)
      #return variance_loss.VarianceError()(y,t)
      #return F.mean_squared_error(y,t)-(F.sum(y-t)**2)/(2*(y.size**2))+F.mean_squared_error(divy_y,divy_t)+F.mean_squared_error(divx_y,divx_t)
    else:
      return y

