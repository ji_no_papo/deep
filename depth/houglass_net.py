import chainer
from chainer import functions as F
from chainer import links as L
import variance_loss
import cupy as xp
import inception_module

class network(chainer.Chain):

  def __init__(self):
    self.ich = 4
    self.och = 1
    self.train = False
    super(network,self).__init__(
      conv1=L.Convolution2D(3,31,3,pad=1),
      inception1=inception_module.Inception(32,4,16,3,7,11),
      inception2=inception_module.Inception(32,8,8,3,5,7),
      inception3=inception_module.Inception(32,8,16,3,7,11),
      inception4=inception_module.Inception(32,16,8,3,5,7),
      inception5=inception_module.Inception(64,16,8,3,5,7),
      inception6=inception_module.Inception(64,16,16,3,7,11),
      inception7=inception_module.Inception(64,8,8,3,5,7),
      conv2=L.Convolution2D(16,1,3,pad=1),
    )

  def __call__(self,x,t):
    if self.train:
      t = chainer.Variable(t)
    rgb,d = chainer.Variable(x[:,:3,:,:]),chainer.Variable(x[:,3,:,:].reshape(len(x),1,len(x[0][0]),len(x[0][0][0])))
    h = F.relu(self.conv1(rgb))
    h = F.concat((h,d),1)
    h1 = F.relu(self.inception1(h))

    h2 = F.relu(self.inception2(F.max_pooling_2d(h,2,stride=2)))
    h2 = F.relu(self.inception2(h2))

    h3 = F.relu(self.inception2(F.max_pooling_2d(h2,2,stride=2)))
    h3 = F.relu(self.inception4(h3))

    h4 = F.relu(self.inception5(F.max_pooling_2d(h3,2,stride=2)))
    h4 = F.relu(self.inception5(h4))

    h5 = F.relu(self.inception5(F.max_pooling_2d(h4,2,stride=2)))
    h5 = F.relu(self.inception5(h5))
    h5 = F.relu(self.inception5(h5))

    h4 = F.relu(self.inception5(h4))
    h4 = F.relu(self.inception5(h4))

    h4 = F.relu(self.inception5(h4+F.unpooling_2d(h5,2,stride=2,outsize=(16,16))))
    h4 = F.relu(self.inception6(h4))

    h3 = F.relu(self.inception5(h3))
    h3 = F.relu(self.inception6(h3))

    h3 = F.relu(self.inception5(h3+F.unpooling_2d(h4,2,stride=2,outsize=(32,32))))
    h3 = F.relu(self.inception7(h3))

    h2 = F.relu(self.inception2(h2))
    h2 = F.relu(self.inception3(h2))

    h2 = F.relu(self.inception2(h2+F.unpooling_2d(h3,2,stride=2,outsize=(64,64))))
    h2 = F.relu(self.inception1(h2))

    y = F.relu(self.conv2(h1+F.unpooling_2d(h2,2,stride=2,outsize=(128,128))))
    if self.train:
      return F.mean_squared_error(y,t)
    else:
      return y

