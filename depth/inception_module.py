from chainer.functions.activation import relu
from chainer.functions.array import concat
from chainer.functions.pooling import max_pooling_2d
from chainer import link
from chainer.links.connection import convolution_2d


class Inception(link.Chain):

    def __init__(self, in_channels, out, proj, conv_size2, conv_size3, conv_size4,
                 conv_init=None, bias_init=None):
      if conv_size3 == 7:
        pad3 = 3
      else:
        pad3 = 2
      if conv_size4 == 11:
        pad4 = 5
      else:
        pad4 = 3
      super(Inception, self).__init__(
        conv1=convolution_2d.Convolution2D(in_channels, out, 1,
                                           initialW=conv_init,
                                           initial_bias=bias_init),
        proj=convolution_2d.Convolution2D(in_channels, proj, 1,
                                           initialW=conv_init,
                                           initial_bias=bias_init),
        conv2=convolution_2d.Convolution2D(proj, out, conv_size2, pad=1,
                                           initialW=conv_init,
                                           initial_bias=bias_init),

        conv3=convolution_2d.Convolution2D(proj, out, conv_size3, pad=pad3,
                                           initialW=conv_init,
                                           initial_bias=bias_init),
        conv4=convolution_2d.Convolution2D(proj, out, conv_size4, pad=pad4,
                                           initialW=conv_init,
                                           initial_bias=bias_init)

      )

    def __call__(self, x):
      out1 = self.conv1(x)
      out2 = self.conv2(relu.relu(self.proj(x)))
      out3 = self.conv3(relu.relu(self.proj(x)))
      out4 = self.conv4(relu.relu(self.proj(x)))
      y = relu.relu(concat.concat((out1, out2, out3, out4), axis=1))
      return y
