import os
import fnmatch
import re
import numpy as np
from chainer import cuda
import pickle
import sys
from tqdm import tqdm

def path2int(path):
  index = os.path.basename(path)
  index = re.search("\d+",index).group()
  #index = int(index.split('.')[0])
  return int(index)

def cmp_to_key(mycmp):
  'Convert a cmp= function into a key= function'
  class K:
    def __init__(self, obj, *args):
      self.obj = obj
    def __lt__(self, other):
      return mycmp(self.obj, other.obj) < 0
    def __gt__(self, other):
      return mycmp(self.obj, other.obj) > 0
    def __eq__(self, other):
      return mycmp(self.obj, other.obj) == 0
    def __le__(self,other):
      return mycmp(self.obj, other.obj) <= 0
    def __ge__(self, other):
      return mycmp(self.obj, other.obj) >= 0
    def __ne__(self, other):
      return mycmp(self.obj, other.obj) != 0
  return K

def cmp(x, y):
  return path2int(x) -  path2int(y)

def find_all_files(directory):
  files = [directory + '/' + path for path in os.listdir(directory)]
  files.sort(key=cmp_to_key(cmp))
  return files

def all_files(path):
  rgb_files = []
  depth_files = []
  files = []
  for file in find_all_files(path):
    if fnmatch.fnmatch(file,'*.ppm'):
      rgb_files.append(file)
    if fnmatch.fnmatch(file,'*.pgm'):
      depth_files.append(file)
    if fnmatch.fnmatch(file,'*depth*.png'):
      depth_files.append(file)
    if fnmatch.fnmatch(file,'*input*.png'):
      rgb_files.append(file)
  rgb_files.sort(key=cmp_to_key(cmp))
  depth_files.sort(key=cmp_to_key(cmp))
  return [rgb_files, depth_files]

def select_files(dataset, now_dataset, batch=1):
  files = []
  for i in range(batch):
    ind = int(len(now_dataset[0]) * np.random.rand())
    files.append((now_dataset[0][ind], now_dataset[1][ind]))
    for j in range(2):
      now_dataset[j].pop(ind)
    if len(now_dataset[0]) == 0:
      for k in range(2):
        now_dataset[k] = dataset[k][:]
  return files

def load_model(path,gpu_flag=0):
  with open(path, 'rb') as i:
    model = pickle.load(i)

  if gpu_flag >= 0:
    cuda.get_device(gpu_flag).use()
    model.to_gpu()
  else:
    model = None
  return model

def load_cont(path):
  f = open(path)
  lines = f.readlines()
  lines.pop(0)
  mat = np.array([[float(i) for i in s.strip("\n").split(" ") if len(i) > 0] for s in lines])
  f.close()
  return mat

def load_patch(path,thred=0.7):
  patch = {}
  f = open(path)
  lines = f.readlines()
  lines.pop(0)
  n = int(lines[0])
  lines.pop(0)
  count = 0
  effect = 0
  print("load patch")
  for i in tqdm(range(1000)):
    lines.pop(0)
    locate = np.array(lines[0].strip("\n").split(" ")).astype(float)
    lines.pop(0)
    normal = np.array(lines[0].strip("\n").split(" ")[0:3]).astype(float)
    lines.pop(0)
    score = float(lines[0].split(" ")[0])
    lines.pop(0)
    lines.pop(0)
    cameras = lines[0].strip("\n").split(" ")
    if score >= thred:
      effect += 1
      for j in cameras[:-1]:
        if j not in patch:
          patch[j] = []
        camera = [int(i) for i in cameras[:-1]]
        patch[j].append({"locate":locate,"normal":normal,"cameras":cameras[:-1]})
    lines.pop(0)
    lines.pop(0)
    lines.pop(0)
    lines.pop(0)
  f.close()
  print("effect points:"+str(effect))
  return patch


def frand_truth(ind):
  return

if __name__ == '__main__':
  select_files()
