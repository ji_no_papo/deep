import numpy as np
import cv2

def in_mat(coord,shape):
  return coord[0] >= 0 and coord[0] < shape[0] and coord[1] >= 0 and coord[1] < shape[1]

def affine(D,M):
  a = np.array([
    [M[0][0],M[0][1],M[0][2]],
    [M[1][0],M[1][1],M[1][2]],
    [M[2][0],M[2][1],M[2][2]]
  ])
  b = np.array([
    D[0] - M[0][3],
    D[1] - M[1][3],
    D[2] - M[2][3]
  ])
  return np.linalg.solve(a,b)

def reshape(mat,shape):
  out = np.zeros(shape)
  y_size = int(mat.shape[0]/shape[0])
  x_size = int(mat.shape[1]/shape[1])
  for y in range(shape[0]):
    for x in range(shape[1]):
      i = mat[y*y_size:(y+1)*y_size,x*x_size:(x+1)*x_size]
      non_zero = i.nonzero()
      mean = 0
      if len(non_zero[0]):
        mean = np.mean(i[non_zero])
      out[y][x] = mean
  return out

def trust2weigt(trust,alpah = 1):
  return 1-np.exp(-(alpah*trust)**2)

def tpoint2mat(cont,tpoints,gt,shape,thread=10):
  depth = np.zeros(gt.shape)
  w = np.zeros(gt.shape)
  depths = [np.r_[cont.dot(tp[:4].T),tp[4]] for tpoint in tpoints for tp in tpoint ]
  depths = [[int(d[0]/d[2]),int(d[1]/d[2]),d[2],trust2weigt(d[3])] for d in depths]
  depths = [d for d in depths if in_mat((d[1],d[0]),shape) and d[3] > 0]
  count = 0
  for d in depths:
    y = int(d[1]*100/shape[0])
    x = int(d[0]*100/shape[1])
    if gt[y][x] > 0 and abs(gt[y][x] - d[2]) <= thread:
      count+=1
      depth[y][x] += d[2]
      w[y][x] += 1#d[3]
  return (depth.ravel(),w.ravel())

def depth2point(depth,cont,trusts=None):
  M = cont
  D = [(depth[y][x]*x,depth[y][x]*y,depth[y][x])
       for y in range(0,depth.shape[0],10)
       for x in range(0,depth.shape[1],10)
  ]
  D = [i for i in D if i[2] >= 0]
  point = [affine(d,M) for d in D]
  if trusts is not None:
    trusts = [trusts[y][x] for y in range(0,trusts.shape[0],10) for x in range(0,trusts.shape[1],10)]
    point = np.array([np.r_[p,1,trust] for (trust,p) in zip(trusts,point)])
  return point

def adjast(out,gt):
  anchor = np.nonzero(gt)
  spare_gt = np.array([gt[y][x] for y,x in zip(anchor[0],anchor[1])])
  spare_out = np.array([out[y][x] for y,x in zip(anchor[0],anchor[1])])
  mean_gt = np.mean(spare_gt)
  mean_out = np.mean(spare_out)
  A = np.array([spare_out,np.ones(len(spare_out))]).T
  scale,origin = np.linalg.lstsq(A,spare_gt)[0]
  print(np.mean(abs(spare_gt-(scale*spare_out+origin))))
  return scale*out+origin

def solve(out,gt,trust,cont,tpoints,no_noise_depth,shape,divx,divy,
          unary_key_cost=0.01,unary_other_cost=0.01,smooth_cost=1,multi_cost=0.1):
  #multi_gt,multi_w= tpoint2mat(cont,tpoints,out,shape,thread=10)
  gt_v = gt.ravel()
  out_v = out.ravel()
  mask_v = no_noise_depth.ravel()
  #multi_gt *= multi_cost
  #w = np.zeros((len(gt_v),len(gt_v)))
  #w += np.diag(unary_key_cost*(gt_v != 0))
  #w += np.diag(unary_other_cost*(gt_v == 0))
  w = unary_key_cost*(gt_v != 0)+unary_other_cost*(gt_v == 0)
  #out_v = unary_key_cost*(gt_v != 0)*gt_v+unary_other_cost*(gt_v == 0)*out_v
  #multi_w *= multi_cost
  #multi_w = np.diag(multi_w)
  mask = (mask_v >= 0)*np.ones(len(mask_v))
  solt = (mask_v < 0)*np.ones(len(mask_v))
  solt *= 0.000000001
  A = np.diag(mask*w+solt) + smooth_cost*(divx.T.dot(divx) + divy.T.dot(divy))
  B = mask*w*out_v+solt
  #A = mask.dot(w + multi_w)+solt + smooth_cost*(divx.T.dot(divx) + divy.T.dot(divy))
  #B = mask.dot(w.dot(out_v.T)+multi_gt.T)+mask.dot(np.ones(len(out_v)))*0.000000001
  return np.linalg.solve(A,B)

def make_normal(depth):
  divx = cv2.Sobel(np.asarray(depth,dtype=np.float32),cv2.CV_32F,1,0)
  divy = cv2.Sobel(np.asarray(depth,dtype=np.float32),cv2.CV_32F,0,1)
  normal = [np.cross(np.array([abs(divx[y][x]+0.00001)/(divx[y][x]+0.00001)*1,0,divx[y][x]]),np.array([0,abs(divy[y][x]+0.00001)/(divy[y][x]+0.00001)*1,divy[y][x]]))
            for y in range(0,depth.shape[0],10)
            for x in range(0,depth.shape[1],10)
            if depth[y][x] >= 0]
  normal = [n/np.linalg.norm(n) for n in normal]
  normal = [np.array([n[0],n[1],abs(n[2])]) for n in normal]
  return normal

