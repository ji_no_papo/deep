import numpy as np
import linear
import point2obj

def make_tri(x,y,depth,grid_size):
    x_len = int(depth.shape[1]/grid_size)+1
    index = int(x/grid_size+x_len*y/grid_size)
    if index < int(x_len*(y/grid_size+1))-1 and index < int(depth.shape[0]/grid_size)*x_len:
      tri1 = [index,index+1,index+x_len+1]
      tri2 = [index,index+x_len+1,index+x_len]
      return [tri1,tri2]
    else:
      return 0

def make_obj(depth,rgb,const,path,grid_size,f_name):
  sample_num = 1
  point = [ linear.affine(np.array([x*depth[y][x],y*depth[y][x],depth[y][x]]),const)
            for y in range(0,depth.shape[0],grid_size)
            for x in range(0,depth.shape[1],grid_size)
  ]
  tri = [ make_tri(x,y,depth,grid_size)
          for y in range(0,depth.shape[0],grid_size)
          for x in range(0,depth.shape[1],grid_size)
  ]
  t_point = [[x/rgb.shape[1],y/rgb.shape[0]]
             for y in range(0,rgb.shape[0],grid_size)
             for x in range(0,rgb.shape[1],grid_size)
  ]
  tri = [v for t in tri if t != 0 for v in t ]
  point2obj.write_objs(point,t_point,tri,f_name)
  point2obj.make_mtlib(path,f_name)
