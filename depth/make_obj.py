def write_obj():
  f = open(ply_name,'w')
  f.write("ply\n")
  f.write("format ascii 1.0\n")
  f.write("element vertex "  + str(len(point)) + "\n")
  f.write("property float x\n")
  f.write("property float y\n")
  f.write("property float z\n")
  f.write("property float nx\n")
  f.write("property float ny\n")
  f.write("property float nz\n")
  f.write("property uchar diffuse_red\n")
  f.write("property uchar diffuse_green\n")
  f.write("property uchar diffuse_blue\n")
  f.write("end_header\n")
  for i in range(len(point)):
    for j in range(3):
      f.write(str(point[i][j])+" ")
    for j in range(3):
      f.write(str(normal[i][j])+" ")
    for j in range(3):
      f.write(str(rgb[i][j])+" ")
    f.write("\n")
  f.close()
