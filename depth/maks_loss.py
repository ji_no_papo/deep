import numpy as np
import cupy
from chainer import function as F

class MaskError(F.Function):
  def forward_gpu(self,inputs):
    d,gt = inputs
    kp_list = np.nonzero(x[3])
    my = y
    mt = t
    for i in range(len(x)):
      kp_list = np.nonzero(x[i][3])
      mask = np.zeros(x[i][3].shape)
      for j in range(len(kp_list[0])):
        mask[kp_list[0][j]][kp_list[1][j]] = 1.
        mask = cuda.cupy.asarray(mask)
        my.data[i][0] = mask*y.data[i][0]
        mt.data[i][0] = mask*t.data[i][0]
    squared = d - gt
    squared = squared.ravel()
    squared = squared.dot(squared)/squared.dtype.type(squared.size)
    diff = (d - gt).sum()/d.dtype.type(2.*d.size**2)
    return squared - diff + div,

    def backward(self,inputs,grad_outputs):
        d,gt = inputs
        gy = grad_outputs[0]
        squared = 2.*(d - gt)/d.dtype.type(d.size)
        diff = d/d.dtype.type(2.*d.size**2)
        divx = cupy.ndarray(d.shape,d.dtype)
        for i in range(d.shape[0]):
          divx[i][0] = 2.*cupy.dot((d-gt)[i][0],cupy.dot(self.dx.T,self.dx))/d.size
        divy = cupy.ndarray(d.shape,d.dtype)
        for i in range(d.shape[0]):
          divy[i][0] = 2.*cupy.dot(cupy.dot(self.dy.T,self.dy),(d-gt)[i][0])/d.size
        div = (divx + divy)/divx.dtype.type(divx.size)
        return gy*(squared-diff+divx+divy),-gy*(squared-diff+divx+divy)
