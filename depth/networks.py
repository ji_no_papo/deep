import depth_in_net
import chamfer_dist_net
import CNN
import rbm
import auto_encoder
import auto_encoder_5ch
import variance_loss_net
import space_d_net
import houglass_net
import houglass_full_net
import chamfer_rgb_net

class Net():
  def __init__(self):
    self.networks = {}
    #network list
    self.networks["depth_in_net"] = depth_in_net.network()
    self.networks["chamfer_dist_net"] = chamfer_dist_net.network()
    #self.networks["auto_encoder"] = auto_encoder.network()
    self.networks["variance_loss_net"] = variance_loss_net.network()
    self.networks["space_d_net"] = space_d_net.network()
    self.networks["houglass_net"] = houglass_net.network()
    self.networks["houglass_full_net"] = houglass_full_net.network()
    self.networks["chamfer_rgb_net"] = chamfer_rgb_net.network()

  def network(self,name):
    return self.networks[name]

  def all_network(self):
    return self.networks.keys()
