from paver.easy import *
import sys
import os
sys.path.append( os.path.abspath('.') )
import train
import networks

@task
def network():
  """make network"""
  print"network name:"
  name = raw_input()
  print "input chaner:"
  ich = raw_input()
  print "output chaner:"
  och = raw_input()
  output_network(ich,och,name)

@task
@cmdopts([
    ('add', 'a', "add train"),
    ])
def train_model():
  """train network"""
  print '\033[32m'+"all networks:"+'\033[0m'
  nets = networks.Net().all_network()
  for key in nets:
    print key
  print '\033[32m'+"network name:"+'\033[0m'
  name = raw_input()
  train.run(name,False)

def output_network(ich,och,name):
  f = open(name+"_net.py","w")
  f.write("import chainer\n")
  f.write("from chainer import functions as F\n")
  f.write("from chainer import links as L\n")
  f.write("\n")
  f.write("class network(chainer.Chain):\n")
  f.write("\n")
  f.write("\n")
  f.write("  def __init__(self):\n")
  f.write("    self.ich = ich\n")
  f.write("    self.och = och\n")
  f.write("    super(network,self).__init__()\n")
  f.write("\n")
  f.write("  def __call__(self,x,t):\n")
  f.write("\n")
  f.write("  def loss(self,x,t):\n")
  f.write("\n")
  f.write("  def forward(self,x):\n")
  f.write("\n")
  f.close()
  f = open("networks.py","r+")
  f.seek(0)
  line = f.readline()
  name += "_net"
  lines = "import " + name + "\n"
  while line:
    lines += line
    if "network list" in line:
      lines += "    self.networks[\"" + name + "\"] = " + name + ".network()\n"
    line = f.readline()
  f.seek(0)
  f.write(lines)
  f.close()

