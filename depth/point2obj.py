import linear
import numpy as np
import os.path
from scipy.spatial import Delaunay
import cv2
from skimage import io
import numpy as np

def point2obj(vertex,rgb,cont,path,f_name):
  img = np.copy(rgb)
  vertex[vertex == 0 ] = -1
  d_point = np.where(vertex > 0)
  depth = vertex[d_point]
  point = [[x*d,y*d,d] for y,x,d in zip(d_point[0],d_point[1],depth)]
  t_point = [[x/rgb.shape[1],y/rgb.shape[0]] for y,x in zip(d_point[0],d_point[1])]
  d_point = [[x,y] for y,x in zip(d_point[0],d_point[1])]
  tri = Delaunay(d_point)
  point = [linear.affine(d,cont) for d in point]
  #write_tri_img(d_point,tri,img,f_name)
  #point = linear.depth2point(vertex,cont)
  write_objs(point,t_point,tri.simplices,f_name)
  make_mtlib(path,f_name)

def write_objs(point,rgb,tri,f_name):
  f = open(f_name+'.obj','w')
  f.write('mtllib ./'+ os.path.split(f_name)[1] + '.mtl\n')
  for p in point:
    f.write('v '+str(p[0])+' '+str(p[1])+' '+str(p[2])+'\n')
  for t in rgb:
    f.write('vt '+str(t[0])+' '+str(1-t[1])+'\n')
  for t in tri:
    f.write('f ')
    for p in t[::-1]:
      f.write(str(p+1)+'/'+str(p+1)+'/ ')
    f.write('\n')
  f.close()

def make_mtlib(path,f_name):
  f = open(f_name+'.mtl','w')
  f.write('newmtl mtl\n')
  f.write('map_Kd ./'+os.path.split(path)[1]+'\n')
  f.close()

def write_tri_img(point,tri,rgb,f_name):
  rgb = rgb[::,::,::-1]
  for t in tri.simplices:
    cv2.line(rgb,tuple(point[t[0]]),tuple(point[t[1]]),(0,0,255))
    cv2.line(rgb,tuple(point[t[0]]),tuple(point[t[2]]),(0,0,255))
    cv2.line(rgb,tuple(point[t[1]]),tuple(point[t[2]]),(0,0,255))
  rgb = rgb[::,::,::-1]
  io.imsave(f_name+'.png',rgb/255)
