import chainer
from chainer import functions as F
from chainer import links as L

class rbm(chainer.Chain):
  def __init__(self):
    super(rbm,self).__init__(
      linear1=L.Linear(40000,1000),
      linear2=L.Linear(1000,400),
      linear3=L.Linear(400,100),
      linear4=L.Linear(100,400),
      linear5=L.Linear(400,10000)
    )

  def __call__(self,x,t):
    h,t = chainer.Variable(x),chainer.Variable(t)
    h = F.relu(self.linear1(h))
    h = F.relu(self.linear2(h))
    h = F.relu(self.linear3(h))
    h = F.relu(self.linear4(h))
    y = F.sigmoid(self.linear5(h))
    return F.mean_squared_error(y, t)

  def forward(self,x):
    h = chainer.Variable(x)
    h = F.relu(self.linear1(h))
    h = F.relu(self.linear2(h))
    h = F.relu(self.linear3(h))
    h = F.relu(self.linear4(h))
    return F.sigmoid(self.linear5(h))
