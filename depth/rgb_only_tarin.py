import numpy as np
import chainer
from chainer import cuda, optimizers
import data2matrix as d2m
import input
import pickle
from skimage import io
import rgb_only_net
import math
import twitter

gpu_flag = 0

if gpu_flag >= 0:
  cuda.check_cuda_available()

isize = 100

model = rgb_only_net.network()
xp = cuda.cupy if gpu_flag >= 0 else np
if gpu_flag >= 0:
  cuda.get_device(gpu_flag).use()
  model.to_gpu()

# setup optimizer
#optimizer = optimizers.AdaGrad()
optimizer = optimizers.Adam()
#optimizer = optimizers.RMSprop()
optimizer.setup(model)

n_epoch = 100000
batchsize = 10

tw_api = twitter.init()


for i in range(n_epoch):
  files = input.select_files('data',batchsize)
  train_i,train_o = d2m.train_data(files,isize,i,3)
  train_i = train_i.astype(np.float32)
  train_i = xp.asarray(train_i.reshape(batchsize,3,isize,isize))
  train_o = np.array(train_o).astype(np.float32)
  mask = np.array(train_o).astype(np.float32)
  train_o = xp.asarray(train_o.reshape(batchsize,1,isize,isize))
  mask = xp.asarray(mask.reshape(batchsize,1,isize,isize))
  optimizer.zero_grads()
  loss = optimizer.update(model,train_i,train_o)

  if i%100 == 0:
    test_batch = 5
    files = input.select_files('testdata',test_batch)
    test_i,test_o = d2m.train_data(files,isize,i,3)
    test_i = test_i.astype(np.float32)
    test_o = np.array(test_o).astype(np.float32)
    for j in range(test_batch):
      io.imsave('test/rgb'+str(j)+'.png',(test_i)[j][:3].transpose(1,2,0))
      io.imsave('test/truth'+str(j)+'.png',(test_o)[j][0])
    test_i = xp.asarray(test_i.reshape(test_batch,3,isize,isize))
    test_o = xp.asarray(test_o.reshape(test_batch,1,isize,isize))
    loss = model.loss(test_i,test_o)
    out = model.forward(test_i)
    out.to_cpu()
    for j in range(test_batch):
      io.imsave('test/test'+str(j)+'.png',out.data[j][0])
      '''
      tw_api.update_with_media('test/rgb'+str(j)+'.png')
      tw_api.update_with_media('test/d'+str(j)+'.png')
      tw_api.update_with_media('test/truth'+str(j)+'.png')
      tw_api.update_with_media('test/test'+str(j)+'.png')
      '''
    #tw_api.update_status(str(loss.data))
    print ('l2_loss:',(loss.data))
 

  '''
  if i % 1000 == 0:
    files = input.select_files('danbo')
    test_i = d2m.conv_data(files,isize)
    io.imsave('test/danbo_test_before.png',test_i[0][3])
    #test_i = d2m.conv_data_5ch(files,isize)
    test_i = test_i.astype(np.float32)
    #io.imsave('test/danbo_rgb.png',(test_i)[0][:3].transpose(1,2,0))
    #io.imsave('test/danbo_d.png',(test_i)[0][3])
    test_i = xp.asarray(test_i.reshape(1,4,isize,isize))
    #test_i = xp.asarray(test_i.reshape(1,5,isize,isize))
    out = model.forward(test_i)
    out.to_cpu()
    #out = chainer.functions.reshape(out,(1,1,100,100))
    io.imsave('test/danbo_test.png',out.data[0][0])
    '''
pickle.dump(model,open('model/rgb_only.pkl','wb'),-1)
