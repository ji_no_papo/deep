import numpy as np
import data2matrix as d2m
import input
import math
import scipy.spatial
from skimage import io
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.future import graph
from sklearn.neighbors import NearestNeighbors
from scipy.spatial import distance
from PIL import Image

def weight(d,beta=1,sigma=255.0):
  return math.exp(-beta*d/sigma)

def segment(rgb,depth,seg_num=100):
  seg = slic(rgb,n_segments=seg_num,sigma=1)
  g = graph.RAG(seg,connectivity=2)
  #rgb = rgb/255.
  #depth = depth/255.
  for n in g:
    g.node[n].update({
      'depth_count':0,
      'depth':0.0,
      'rgb_count':0,
      'rgb':np.array([0.0,0.0,0.0]),
      'labels':[n]
    })
  for y in range(depth.shape[0]):
    for x in range(depth.shape[1]):
      ind = seg[y][x]
      g.node[ind]['depth_count'] += 1
      g.node[ind]['depth'] += depth[y][x]
      g.node[ind]['rgb_count'] += 1
      g.node[ind]['rgb'] += rgb[y][x]
  for n in g:
    g.node[n]['depth'] = g.node[n]['depth']/g.node[n]['depth_count']
    g.node[n]['rgb'] = g.node[n]['rgb']/g.node[n]['rgb_count']
  knn_rgb_list = [g.node[n]['rgb'] for n in g]
  rgb = rgb.reshape(rgb.shape[0]*rgb.shape[1],rgb.shape[2])
  knn_rgb = NearestNeighbors(7,metric='mahalanobis',metric_params={'VI':np.cov(rgb, rowvar=0, bias=1)})
  #knn_rgb = NearestNeighbors,7)
  knn_depth_list = [[g.node[n]['depth']] for n in g]
  knn_depth = NearestNeighbors(10)
  knn_rgb.fit(knn_rgb_list)
  knn_depth.fit(knn_depth_list)
  #seg_depth = np.zeros(depth.shape)
  #for y in range(depth.shape[0]):
  #  for x in range(depth.shape[1]):
  #    seg_depth[y][x] = g.node[seg[y][x]]['depth']
  #io.imsave('seg_depth.png',seg_depth/255.)
  for x, y, d in g.edges_iter(data=True):
    i = ([g.node[x]['rgb']],[[g.node[x]['depth']]])
    j = ([g.node[y]['rgb']],[[g.node[y]['depth']]])
    dist = scipy.spatial.distance.mahalanobis(i[0][0],j[0][0],np.cov(rgb, rowvar=0, bias=1))
    #diff = np.array(i[0][0]) - np.array(j[0][0])
    #dist = np.sqrt(np.dot(diff,diff))
    dist = dist**2
    rgb_w = weight(dist,sigma=knn_rgb.kneighbors(i[0])[0][0][-1]*knn_rgb.kneighbors(j[0])[0][0][-1])
    d_diff = np.array(i[1][0]) - np.array(j[1][0])
    d_dist = np.sqrt(np.dot(d_diff,d_diff))
    d_dist = d_dist**2
    depth_w = weight(d_dist,sigma=knn_depth.kneighbors(i[1])[0][0][-1]*knn_depth.kneighbors(j[1])[0][0][-1])
    #d['weight'] = rgb_w*depth_w
    #print(knn_rgb.kneighbors(i[0])[0][0][-1])
    #print(rgb_w)
    d['weight'] = depth_w
  seg = graph.ncut(seg,g)
  return seg

def make_img(rgb_path,depth_path):
  rgb_path = input.find_all_files(rgb_path)
  depth_path = input.find_all_files(depth_path)
  for i in range(len(rgb_path)):
    depth = np.array(Image.open(depth_path[i]))/255.
    rgb = np.array(Image.open(rgb_path[i]).resize(depth.shape))
    seg = segment(rgb,depth)
    io.imsave('seg'+str(i)+'.png',mark_boundaries(rgb,seg))


if __name__ == '__main__':
  import warnings
  import input
  rgb_path = input.find_all_files('danbo/rgb')
  depth_path = input.find_all_files('danbo/dence_depth')
  for i in range(len(rgb_path)):
    depth = np.array(Image.open(depth_path[i]))/255
    rgb = np.array(Image.open(rgb_path[i]).resize(depth.shape))
    seg = segment(rgb,depth)
    with warnings.catch_warnings():
      warnings.simplefilter("ignore")
      io.imsave('seg'+str(i)+'.png',mark_boundaries(rgb,seg))
      #io.imsave('spx.png',mark_boundaries(rgb,slic(rgb,n_segments=100)))
