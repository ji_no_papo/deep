import input
import numpy as np
from skimage import io
import cv2
from tqdm import tqdm

def patch2depth(cont,patchs,shape):
  depth = np.zeros(shape)
  patchs = np.array([(cont.dot(patch["locate"].T).reshape(3),len(patch["cameras"])) for patch in patchs])
  points = np.array([(patch[0]/patch[0][2]).astype(int) for patch in patchs])
  d = [patch[0][2] for patch in patchs]
  depth[(points.T[1],points.T[0])] = d
  return depth

def sfm2img(cont_dir_path,patch_path,rgb_dir_path,out_dir):
  cont_list = input.find_all_files(cont_dir_path)
  rgb_list = input.find_all_files(rgb_dir_path)
  conts = [input.load_cont(path) for path in cont_list]
  rgbs = [np.array(cv2.imread(path))[::,::,::-1] for path in rgb_list]
  #rgbs = [np.rot90(rgb) for rgb in rgbs]
  patchs = input.load_patch(patch_path)
  index = [int(i) for i in sorted(patchs.keys())]
  rgbs = [rgbs[key] for key in index]
  conts = [conts[key] for key in index]
  patchs = [patchs[key] for key in sorted(patchs.keys())]
  depths = [patch2depth(cont,patch,rgb.shape[:2]) for (cont,patch,rgb) in tqdm(zip(conts,patchs,rgbs))]
  [io.imsave(out_dir+'/in_depth'+str(i).zfill(4)+'.png',depth/255.) for i,depth in zip(index,depths)]
