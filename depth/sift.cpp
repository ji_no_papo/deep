#include<iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/nonfree.hpp>

int main(){
  cv::Mat img = cv::imread("/home/ji-no/deep/depth/data/bedroom_0135/r-1316568984.147757-397086849.ppm");

  cv::initModule_nonfree();
  cv::Ptr<cv::FeatureDetector> detector = cv::FeatureDetector::create("SIFT");
  std::vector<cv::KeyPoint> keypoint;
  detector->detect(img, keypoint);
  std::cout << keypoint.size() << std::endl;
  for(auto var:keypoint){
    img.at<cv::Vec3b>((int)var.pt.y,(int)var.pt.x) = cv::Vec3b(255,0,0);
  }
  cv::imwrite("sift.pgm",img);
  return 0;
}
