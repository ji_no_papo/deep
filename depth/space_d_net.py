import chainer
from chainer import functions as F
from chainer import links as L
from chainer import cuda
import numpy as np
import cupy
import variance_loss

class network(chainer.Chain):

  def __init__(self):
    self.ich = 4
    self.och = 1
    super(network,self).__init__(
      en_conv1=L.Convolution2D(4,16,5,pad=2),
      en_conv2=L.Convolution2D(16,32,5,pad=2),
      en_conv3=L.Convolution2D(32,16,5,pad=2),
      en_conv4=L.Convolution2D(16,1,5,pad=2),
    )

  def __call__(self,x,t):
    hx,t = chainer.Variable(x),chainer.Variable(t)
    h = F.relu(self.en_conv1(hx))
    h = F.relu(self.en_conv2(h))
    h = F.relu(self.en_conv3(h))
    y = F.sigmoid(self.en_conv4(h))
    #return variance_loss.VarianceError()(y,t)
    return F.mean_squared_error(y, t)

  def loss(self,x,t):
    hx,t = chainer.Variable(x),chainer.Variable(t)
    h = F.relu(self.en_conv1(hx))
    h = F.relu(self.en_conv2(h))
    h = F.relu(self.en_conv3(h))
    y = F.sigmoid(self.en_conv4(h))
    #return variance_loss.VarianceError()(y,t)
    return F.mean_squared_error(y, t)


  def forward(self,x):
    h = chainer.Variable(x)
    h = F.relu(self.en_conv1(h))
    h = F.relu(self.en_conv2(h))
    h = F.relu(self.en_conv3(h))
    y = F.sigmoid(self.en_conv4(h))
    return y

  '''
  def __init__(self):
    super(network,self).__init__(
      en_conv1=L.Convolution2D(4,32,5,pad=2),
      en_conv2=L.Convolution2D(32,32,5,pad=2),
      en_conv3=L.Convolution2D(32,32,5,pad=2),
      en_conv4=L.Convolution2D(32,32,5,pad=2),
      en_conv5=L.Convolution2D(32,32,5,pad=2),
      en_conv6=L.Convolution2D(32,1,5,pad=2),
    )

  def __call__(self,x,t):
    h,t = chainer.Variable(x),chainer.Variable(t)
    h = F.sigmoid(self.en_conv1(h))
    h = F.sigmoid(self.en_conv2(h))
    h = F.sigmoid(self.en_conv3(h))
    h = F.sigmoid(self.en_conv4(h))
    h = F.sigmoid(self.en_conv5(h))
    y = F.sigmoid(self.en_conv6(h))
    kp_list = np.nonzero(x[3])
    my = cupy.zeros(y.data.shape,y.data.dtype)
    mt = cupy.zeros(t.data.shape,y.data.dtype)
    for i in range(len(x)):
      kp_list = np.nonzero(x[i][3])
      mask = np.zeros(x[i][3].shape)
      for j in range(len(kp_list[0])):
        mask[kp_list[0][j]][kp_list[1][j]] = 1.
      mask = cuda.cupy.asarray(mask)
      my[i][0] = mask*y.data[i][0]
      mt[i][0] = mask*t.data[i][0]
    my,mt = chainer.Variable(my),chainer.Variable(mt)
    return variance_loss.VarianceError()(y,t) + F.mean_squared_error(my, mt)

  def loss(self,x,t):
    h,t = chainer.Variable(x),chainer.Variable(t)
    h = F.sigmoid(self.en_conv1(h))
    h = F.sigmoid(self.en_conv2(h))
    h = F.sigmoid(self.en_conv3(h))
    h = F.sigmoid(self.en_conv4(h))
    h = F.sigmoid(self.en_conv5(h))
    y = F.sigmoid(self.en_conv6(h))
    return F.mean_squared_error(y, t)


  def forward(self,x):
    h = chainer.Variable(x)
    h = F.sigmoid(self.en_conv1(h))
    h = F.sigmoid(self.en_conv2(h))
    h = F.sigmoid(self.en_conv3(h))
    h = F.sigmoid(self.en_conv4(h))
    h = F.sigmoid(self.en_conv5(h))
    y = F.sigmoid(self.en_conv6(h))
    return y
'''
