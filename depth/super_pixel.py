import numpy as np
from skimage.segmentation import slic
from skimage.future import graph
from sklearn.neighbors import NearestNeighbors
import scipy.spatial
import math

def weight(dist,beta=1,sigma=255.0):
  return math.exp(-beta*dist/sigma)

def make_graph(seg,seg_num):
  graph = [[] for i in range(seg_num)]
  for y in range(seg.shape[0]-1):
    for x in range(seg.shape[1]-1):
      if seg[x][y] != seg[x+1][y] and seg[x+1][y] not in graph[seg[x][y]]:
        graph[seg[x][y]].append(seg[x+1][y])
        graph[seg[x+1][y]].append(seg[x][y])
      if seg[x][y] != seg[x][y+1] and seg[x][y+1] not in graph[seg[x][y]]:
        graph[seg[x][y]].append(seg[x][y+1])
        graph[seg[x][y+1]].append(seg[x][y])
  return graph

def fill_spx(rgb,sparse,seg_num=100,sigma=1):
  seg = slic(rgb,n_segments=seg_num,sigma=sigma)
  g = graph.RAG(seg,connectivity=2)
  out = np.zeros(sparse.shape)
  for n in g:
    g.node[n].update({
      'coord_list':[]
    })
  keys = sparse.nonzero()
  for y,x in zip(keys[0],keys[1]):
    ind = seg[y][x]
    g.node[ind]['coord_list'].append([y,x])
  for y in range(sparse.shape[0]):
    for x in range(sparse.shape[1]):
      ind = seg[y][x]
      label = -1
      nearest = 1000000
      for i in g.node[ind]['coord_list']:
        diff = np.array(i) - np.array([y,x])
        dist = np.sqrt(diff.dot(diff))
        if nearest > dist:
          nearest = dist
          label = sparse[y][x]
      if label >= 0:
        out[y][x] = label
  return out

def fill_graph_cut(g,thred = 0.001):
  fill = 1
  count = 0
  while(fill != count):
    count = fill
    fill = 0
    for n in g:
      if not g.node[n]['fill']:
        continue
      fill += 1
      for neg in g.edge[n]:
        if g.node[neg]['fill'] or n == neg:
          continue
        if g.edge[n][neg]['weight'] > thred:
          g.node[neg]['depth_count'] += 1
          g.node[neg]['depth'] += g.node[n]['depth']
    for n in g:
      if not g.node[n]['fill'] and g.node[n]['depth_count'] > 0:
        g.node[n]['depth'] /= g.node[n]['depth_count']
        g.node[n]['fill'] = True
  return g

def fill_mean(g):
  fill = 1
  count = 0
  while(fill != count):
    count = fill
    fill = 0
    for n in g:
      if not g.node[n]['fill']:
        continue
      sumw = 0
      fill += 1
      for neg in g.edge[n]:
        if n == neg:
          continue
        sumw += g.edge[n][neg]['weight']
      for neg in g.edge[n]:
        if g.node[neg]['fill'] or n == neg:
          continue
        g.node[neg]['depth_count'] += 1
        g.node[neg]['depth'] += g.node[n]['depth']
    for n in g:
      if not g.node[n]['fill'] and g.node[n]['depth_count'] > 0:
        g.node[n]['depth'] /= g.node[n]['depth_count']
        g.node[n]['fill'] = True
  return g

def make_near_depth(rgb,depth,kp_list=None):
  from skimage import io
  seg_num = 100
  seg = slic(rgb,n_segments=seg_num,sigma=1)
  g = graph.RAG(seg,connectivity=2)
  in_depth = np.zeros(depth.shape)
  #rgb = rgb/255.
  #depth = depth/255.
  for n in g:
    g.node[n].update({
      'depth_count':0,
      'depth':0.0,
      'rgb_count':0,
      'rgb':np.array([0.0,0.0,0.0]),
      'fill':False
    })
  if(kp_list):
    for key in kp_list:
      in_depth[int(key.pt[1])][int(key.pt[0])] = depth[int(key.pt[1])][int(key.pt[0])]
      ind = seg[int(key.pt[1])][int(key.pt[0])]
      g.node[ind]['depth_count'] += 1
      g.node[ind]['depth'] += depth[int(key.pt[1])][int(key.pt[0])]
  else:
    for y in range(depth.shape[0]):
      for x in range(depth.shape[1]):
        if depth[y][x] != 0:
          ind = seg[y][x]
          g.node[ind]['depth_count'] += 1
          g.node[ind]['depth'] += depth[y][x]
  for n in g:
    rgb_mean = [np.mean(rgb[seg == n].T[0]),np.mean(rgb[seg == n].T[1]),np.mean(rgb[seg == n].T[1])]
    g.node[n]['rgb'] = rgb_mean
  for n in g:
    if g.node[n]['depth_count'] != 0:
      g.node[n]['depth'] /= g.node[n]['depth_count']
      g.node[n]['fill'] = True
  knn_list = [g.node[n]['rgb'] for n in g]
  rgb = rgb.reshape(rgb.shape[0]*rgb.shape[1],rgb.shape[2])
  #knn = NearestNeighbors(7,metric='mahalanobis',metric_params={'VI':np.cov(rgb, rowvar=0, bias=1)})
  knn = NearestNeighbors(7)
  knn.fit(knn_list)
  for x, y, d in g.edges_iter(data=True):
    i = ([g.node[x]['rgb']],[[g.node[x]['depth']]])
    j = ([g.node[y]['rgb']],[[g.node[y]['depth']]])
    sigma = knn.kneighbors(i[0])[0][0][-1]*knn.kneighbors(j[0])[0][0][-1]
    if(sigma == 0):
      sigma = 0.000001
    diff = np.array(g.node[x]['rgb']) - np.array(g.node[y]['rgb'])
    dist = np.sqrt(diff.dot(diff))
    #dist = scipy.spatial.distance.mahalanobis(i[0][0],j[0][0],np.cov(rgb, rowvar=0, bias=1))
    dist = dist**2
    d['weight'] = weight(dist,sigma=sigma)
  g = fill_graph_cut(g)
  g = fill_mean(g)
  for n in g:
    in_depth[seg == n] = g.node[n]['depth']
  return in_depth

if __name__ == '__main__':
  import input
  import cv2
  from PIL import Image
  from skimage import io
  import time
  size = 100
  testdataset = input.all_files('testdata')
  #testdataset = (['data/38830.ppm'],['data/38830.pgm'])
  sum_loss = 0.
  sum_rms = 0.
  times = 0.
  for j in range(len(testdataset[0])):
    files = [(testdataset[0][j],testdataset[1][j])]
    rgb = np.array(Image.open(testdataset[0][j]).resize((size,size)))
    depth = np.array(Image.open(testdataset[1][j]).resize((size,size)))
    gftt = cv2.xfeatures2d.SIFT_create()
    kp_list = gftt.detect(rgb)
    start = time.time()
    in_depth = make_near_depth(rgb,depth,kp_list)
    in_depth = (in_depth-in_depth.min())/(in_depth.max()-in_depth.min())
    #in_depth = make_near_depth(rgb,depth)
    times += time.time() - start
    io.imsave('test/test'+str(j)+'.png',(in_depth))
    diff = in_depth/255. - depth/255.
    diff = diff.ravel()
    sum_rms += np.sqrt(255.*np.array(diff.dot(diff) / diff.size))
  print('rms_loss:',sum_rms/len(testdataset[0]))
  print('ave_time',times/len(testdataset[0]))
