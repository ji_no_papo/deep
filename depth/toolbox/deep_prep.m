imgRgb = imread();
imgDepth = imread();

imgRgb = crop_image(imgRgn);
imgDepth = crop_image(imgDepth);
imgDepthFiled = fill_depth_cross_bf(imgRgb, double(imgDepthAbs));

figure(1);
subplot(1,3,1); imagesc(imgRgb);
subplot(1,3,2); imagesc(imgDepth);
subplot(1,3,3); imagesc(imgDepthFilled);
