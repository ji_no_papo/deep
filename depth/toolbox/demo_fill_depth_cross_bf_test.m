% Demo's the in-painting function fill_depth_cross_bf.m

%DATASET_PATH = '~/test/labeled_data.mat';

%load(DATASET_PATH, 'images', 'rawDepths');

images = imread('/home/ji-no/deep/depth/data/bedroom_0135/r-1316568984.147757-397086849.ppm');
rewDepths = imread('/home/ji-no/deep/depth/data/bedroom_0135/d-1316568984.168719-398303518.pgm');

% Crop the images to include the areas where we have depth information.
imgRgb = crop_image(images);
imgDepthAbs = crop_image(rewDepths);
imgDepthAbs = im2double(imgDepthAbs);

imgDepthFilled = fill_depth_cross_bf(imgRgb, imgDepthAbs);

figure(1);
subplot(1,3,1); imagesc(imgRgb);
subplot(1,3,2); imagesc(imgDepthAbs);
subplot(1,3,3); imagesc(imgDepthFilled);