out_dir = '/home/ji-no/deep/depth/data';
root_dir = '../data';
dirs = dir(root_dir);
index = 0;
for i = 3:size(dirs)
    dir_name = strcat('../data/',dirs(i).name,'/');
    index_path = dir(strcat(dir_name,'*.txt'));
    file = strcat(dir_name,index_path(1).name);
    fileId = fopen(file);
    indexfile = textscan(fileId,'%s');
    %index = 11167;
    %celldisp(index);
    rgb_flag = false;
    depth_flag = false;
    for ind = 1:size(indexfile{1},1)
        str = indexfile{1}{ind};
        if rgb_flag == false
            if(numel(strfind(str,'ppm')) ~= 0)
                rgb_img = imread(strcat(dir_name,str));
                rgb_flag = true;
            end 
        elseif depth_flag == false
            if(numel(strfind(str,'pgm')) ~= 0)
                depth_img = imread(strcat(dir_name,str));
                depth_flag = true;
            end 
        end
        if rgb_flag ~= false && depth_flag ~= false
            rgb_img = crop_image(rgb_img);
            depth_img = crop_image(depth_img);
            depth_img = im2double(depth_img);
            fill_img = fill_depth_cross_bf(rgb_img, depth_img); 
            imwrite(rgb_img,strcat(out_dir,num2str(index),'.ppm'));
            imwrite(depth_img,strcat(out_dir,num2str(index),'.pgm'));
            imwrite(fill_img,strcat(out_dir,'fill',num2str(index),'.pgm'));
            rgb_flag = false;
            depth_flag = false;
            index = index + 1;
        end
    end
end
