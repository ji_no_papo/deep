function I = im2double(img)
  s = size(img);
  m = max(img(:));
  I = zeros(s(1,1),s(1,2),'double');
  for x = 1:s(1,1)
    for y = 1:s(1,2)
        I(x,y) = double(img(x,y))/double(m);
    end
  end
end
