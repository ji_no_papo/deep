function gray = rgb2gray(img)
  s = size(img);
  gray = zeros(s(1,1),s(1,2),'uint8');
  for x = 1:s(1,1)
    for y = 1:s(1,2)
      r = img(x,y,1);
      g = img(x,y,2);
      b = img(x,y,3);
      gray(x,y) = uint8(0.2989*r+0.5870*g+0.1140*b);
    end
  end
end
