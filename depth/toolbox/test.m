images = imread('/home/ji-no/deep/depth/data/bedroom_0135/r-1316569011.285431-2025320874.ppm');
rewDepths = imread('/home/ji-no/deep/depth/data/bedroom_0135/d-1316569011.391819-2026055533.pgm');

imgRgb = crop_image(images);
imgDepthAbs = crop_image(rewDepths);

I = double(imgDepthAbs);

figure(1);
imagesc(imgDepthAbs);

for i = 1:100
    I = fill_depth_colorization(im2double(imgRgb),I,0.25);
    figure(2);
    imagesc(I);
end