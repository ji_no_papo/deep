import numpy as np
import chainer
from chainer import cuda, optimizers
import data2matrix as d2m
import input
import pickle
from skimage import io
import twitter
import sys
import networks
import houglass_net
import use_model
import matplotlib.pyplot as plt
from tqdm import tqdm
from PIL import Image
import random

def run(model_name,net_name=None,batchsize=8):
  gpu_flag = 0

  if gpu_flag >= 0:
    cuda.check_cuda_available()

  isize = 128

  if net_name:
    model = networks.Net().networks[net_name]
  else:
    model = input.load_model('model/'+model_name+'.pkl')
  model.train = True
  xp = cuda.cupy if gpu_flag >= 0 else np
  if gpu_flag >= 0:
    cuda.get_device(gpu_flag).use()
    model.to_gpu()

  # setup optimizer
  #optimizer = optimizers.AdaGrad()
  optimizer = optimizers.Adam()
  #optimizer = optimizers.MomentumSGD(lr=0.001)
  #optimizer = optimizers.RMSprop()
  optimizer.setup(model)

  tw_api = twitter.init()
  dataset = input.all_files('data')
  inputdataset = input.find_all_files('inputdata')
  dataset = [dataset[0],dataset[1],inputdataset]
  now_dataset = [[],[],[]]
  for i in range(3):
    now_dataset[i] = dataset[i][:]
  print("dataset:" + str(len(dataset[0])))
  testdataset = input.all_files('testdata')
  files = [i for i in zip(testdataset[0],testdataset[1])]
  testdataset = d2m.train_data(files,isize,xp,False,model.ich)
  print("testdataset:" + str(len(testdataset[0])))
  epoch = 0
  test_loss_sum = []
  test_log = []
  train_log = []

  while True:

    if epoch%(batchsize*10000) == 0:
      test_log.append(use_model.cross_validation(model,testdataset,xp,isize))
      if len(test_loss_sum) == 0:
        test_loss_sum.append(0)
      print('train_loss:',np.mean(test_loss_sum))
      train_log.append(np.mean(test_loss_sum))
      pickle.dump(model,open('model/'+model_name+'.pkl','wb'),-1)
      test_loss_sum = []
      plt.plot(test_log,label="test")
      plt.plot(train_log,label="train")
      plt.savefig("log.png")

    files = input.select_files(dataset,now_dataset,batchsize)
    #train_i,train_o = d2m.img_load(files,isize,xp,model.ich)
    train_i,train_o = d2m.train_data(files,isize,xp,True,model.ich)
    optimizer.zero_grads()
    optimizer.update(model,train_i,train_o)
    epoch += batchsize
    loss = model(train_i,train_o)
    loss.to_cpu()
    test_loss = loss.data
    test_loss_sum.append(test_loss)
    sys.stdout.write("\r%d:%f" % (epoch,test_loss))
    sys.stdout.flush()
    if epoch >= len(dataset[0]):
      epoch = 0


