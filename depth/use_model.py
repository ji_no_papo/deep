from chainer import cuda
import input
import data2matrix as d2m
from skimage import io
import chainer
import numpy as np
import sys

def reshape(mat,shape):
  out = np.zeros(shape)
  y_size = int(mat.shape[0]/shape[0])
  x_size = int(mat.shape[1]/shape[1])
  for y in range(shape[0]):
    for x in range(shape[1]):
      i = mat[y*y_size:(y+1)*y_size,x*x_size:(x+1)*x_size]
      non_zero = i.nonzero()
      mean = 0
      if len(non_zero[0]) > 0:
        mean = np.mean(i[non_zero])
      out[y][x] = mean
  return out

def make_img(model_path,rgb_path,depth_path,out_dir_path):
  model = input.load_model(model_path)
  rgb = input.find_all_files(rgb_path)
  depth = input.find_all_files(depth_path)
  for i in range(len(rgb)):
    files = [(rgb[i],depth[i])]
    in_data,gt = d2m.train_data(files,128,cuda.cupy,False,model.ich)
    model.train = False
    out = model(in_data,None)
    out.to_cpu()
    in_data = chainer.cuda.to_cpu(in_data)
    out.data[0][0] = out.data[0][0]/out.data[0][0].max()
    io.imsave(out_dir_path + '/test'+str(i).zfill(4)+'.png',out.data[0][0])
    #io.imsave(out_dir_path + '/test'+str(i).zfill(4)+'.png',in_data[0][3])

def cross_validation(model,dataset,xp,isize):
  sum_loss = 0.
  sum_rms = 0.
  for j in range(len(dataset[0])):
    in_data = dataset[0][j]
    in_data = in_data.reshape(1,in_data.shape[0],in_data.shape[1],in_data.shape[2])
    gt = dataset[1][j]
    gt = gt.reshape(1,gt.shape[0],gt.shape[1],gt.shape[2])
    loss = model(in_data,gt).data
    model.train = False
    out = model(in_data,None)
    model.train = True
    out.to_cpu()
    sum_loss = sum_loss + loss
    pred = out.data[0][0]
    pred = (pred-pred.min())/(pred.max()-pred.min())
    io.imsave('test/test'+str(j)+'.png',pred)
    gt = chainer.cuda.to_cpu(gt)
    gt = (gt-gt.min())/(gt.max()-gt.min())
    io.imsave('test/truth'+str(j)+'.png',gt[0][0])
    diff = pred - gt
    diff = diff.ravel()
    sum_rms += np.sqrt(255.*np.array(diff.dot(diff) / diff.size))
    sys.stdout.write("\r%d:%f" % (j,loss))
    sys.stdout.flush()
  print("")
  print('rms_loss:',sum_rms/len(dataset[0]))
  print('test_loss:',sum_loss/len(dataset[0]))
  return sum_loss/len(dataset[0])


