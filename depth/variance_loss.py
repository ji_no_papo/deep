import numpy as np
from chainer.cuda import cupy
from chainer import function as F
from chainer.functions import math

class VarianceError(F.Function):

    def forward_gpu(self,inputs):
        self.d,gt = inputs
        self.mask = self.d > 0
        self.d *= self.mask
        gt *= self.mask
        gt = cupy.where(self.d==0,cupy.ones(self.d.shape,dtype=self.d.dtype),gt)
        gt = cupy.log(gt)
        self.d = cupy.where(self.d==0,cupy.ones(self.d.shape,dtype=self.d.dtype),self.d)
        d = cupy.log(self.d)
        self.diff = d - gt
        self.divx_mask = self.mask[:,:,1:,:] * self.mask[:,:,:-1,:]
        self.divy_mask = self.mask[:,:,:,1:] * self.mask[:,:,:,:-1]
        self.diff_divx = ((d[:,:,1:,:] - d[:,:,:-1,:]) - (gt[:,:,1:,:] - gt[:,:,:-1,:]))*self.divx_mask
        self.diff_divy = ((d[:,:,:,1:] - d[:,:,:,:-1]) - (gt[:,:,:,1:] - gt[:,:,:,:-1]))*self.divy_mask
        #self.diff = d - gt
        divx = self.diff_divx.ravel()
        divx = divx.dot(divx)/self.divx_mask.sum()
        divy = self.diff_divy.ravel()
        divy = divy.dot(divy)/self.divy_mask.sum()
        div = divx+divy
        squared = self.diff.ravel()
        squared = squared.dot(squared)/self.mask.sum()
        sqrt_diff = self.diff.sum()**2/(2.*(self.mask.sum()**2))
        return squared - sqrt_diff + div,

    def backward(self,inputs,grad_outputs):
        grad = 1/self.d
        #grad = 1
        gy = grad_outputs[0]
        n = self.mask.sum(dtype=self.diff.dtype)
        g = 2.*n/(n**2)*((n+1)*self.diff-self.diff.sum(dtype=self.diff.dtype))
        g[:,:,1:,:] += 2.*self.diff_divx/self.divy_mask.sum(dtype=self.diff.dtype)
        g[:,:,:,1:] += 2.*self.diff_divy/self.divx_mask.sum(dtype=self.diff.dtype)
        g *= grad
        return g,-g
