import chainer
from chainer import functions as F
from chainer import links as L
import variance_loss

class network(chainer.Chain):
  def __init__(self):
    self.ich = 3
    self.och = 1
    super(network,self).__init__(
      en_conv1=L.Convolution2D(3,32,5,pad=2),
      en_conv2=L.Convolution2D(32,32,5,pad=2),
      en_conv3=L.Convolution2D(32,32,5,pad=2),
      en_conv4=L.Convolution2D(32,32,5,pad=2),
      en_conv5=L.Convolution2D(32,32,5,pad=2),
      en_conv6=L.Convolution2D(32,1,5,pad=2),
    )

  def __call__(self,x,t):
    hx,t = chainer.Variable(x),chainer.Variable(t)
    h = F.sigmoid(self.en_conv1(hx))
    h = F.sigmoid(self.en_conv2(h))
    h = F.sigmoid(self.en_conv3(h))
    h = F.sigmoid(self.en_conv4(h))
    h = F.sigmoid(self.en_conv5(h))
    y = F.sigmoid(self.en_conv6(h))
    return variance_loss.VarianceError()(y,t)

  def loss(self,x,t):
    hx,t = chainer.Variable(x),chainer.Variable(t)
    h = F.sigmoid(self.en_conv1(hx))
    h = F.sigmoid(self.en_conv2(h))
    h = F.sigmoid(self.en_conv3(h))
    h = F.sigmoid(self.en_conv4(h))
    h = F.sigmoid(self.en_conv5(h))
    y = F.sigmoid(self.en_conv6(h))
    return variance_loss.VarianceError()(y,t)


  def forward(self,x):
    h = chainer.Variable(x)
    h = F.sigmoid(self.en_conv1(h))
    h = F.sigmoid(self.en_conv2(h))
    h = F.sigmoid(self.en_conv3(h))
    h = F.sigmoid(self.en_conv4(h))
    h = F.sigmoid(self.en_conv5(h))
    y = F.sigmoid(self.en_conv6(h))
    return y
