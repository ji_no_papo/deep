import numpy as np
import chainer
from chainer import cuda, optimizers
import data2matrix as d2m
import input
import pickle
from skimage import io
import CNN
import rbm
import variance_loss_net
import math
import twitter
import sys

gpu_flag = 0

if gpu_flag >= 0:
  cuda.check_cuda_available()

isize = 100

model = variance_loss_net.network()
xp = cuda.cupy if gpu_flag >= 0 else np
if gpu_flag >= 0:
  cuda.get_device(gpu_flag).use()
  model.to_gpu()

# setup optimizer
#optimizer = optimizers.AdaGrad()
#optimizer = optimizers.Adam()
optimizer = optimizers.AdaDelta()
#optimizer = optimizers.RMSprop()
optimizer.setup(model)

batchsize = 15

tw_api = twitter.init()
dataset = input.all_files('data')
testdataset = input.all_files('testdata')
epoch = -1

while True:
  files = input.select_files(dataset,batchsize)
  train_i,train_o = d2m.train_data(files,isize,xp,3)
  optimizer.zero_grads()
  optimizer.update(model,train_i,train_o)
  epoch += 1
  sys.stdout.write("\r%d" % epoch)
  sys.stdout.flush()

  if epoch%1000 == 0:
    epoch = 0
    sum_loss = 0
    for j in range(len(testdataset[0])):
      files = [(testdataset[0][j],testdataset[1][j],testdataset[2][j])]
      test_i,test_o = d2m.train_data(files,isize,xp,3)
      io.imsave('test/testdata_rgb_var/rgb'+str(j)+'.png',chainer.cuda.to_cpu(test_i)[0][:3].transpose(1,2,0)/1.2)
      io.imsave('test/testdata_rgb_var/truth'+str(j)+'.png',chainer.cuda.to_cpu(test_o)[0][0])
      loss = model.loss(test_i,test_o)
      out = model.forward(test_i)
      out.to_cpu()
      io.imsave('test/testdata_rgb_var/test'+str(j)+'.png',out.data[0][0])
      sum_loss = sum_loss + loss.data
      '''
      tw_api.update_with_media('test/rgb'+str(j)+'.png')
      tw_api.update_with_media('test/d'+str(j)+'.png')
      tw_api.update_with_media('test/truth'+str(j)+'.png')
      tw_api.update_with_media('test/test'+str(j)+'.png')
      '''
    #tw_api.update_status(str(loss.data))
    print ('l2_loss:',(sum_loss/len(testdataset[0])))
    pickle.dump(model,open('model/rgb_vriance_more.pkl','wb'),-1)
  '''
  if i % 1000 == 0:
    files = input.select_files('danbo')
    test_i = d2m.conv_data(files,isize)
    io.imsave('test/danbo_test_before.png',test_i[0][3])
    #test_i = d2m.conv_data_5ch(files,isize)
    test_i = test_i.astype(np.float32)
    #io.imsave('test/danbo_rgb.png',(test_i)[0][:3].transpose(1,2,0))
    #io.imsave('test/danbo_d.png',(test_i)[0][3])
    test_i = xp.asarray(test_i.reshape(1,4,isize,isize))
    #test_i = xp.asarray(test_i.reshape(1,5,isize,isize))
    out = model.forward(test_i)
    out.to_cpu()
    #out = chainer.functions.reshape(out,(1,1,100,100))
    io.imsave('test/danbo_test.png',out.data[0][0])
    '''

