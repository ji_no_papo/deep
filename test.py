import tensorflow as tf
import time
import input_data
import math

BATCH_SIZE = 150
LOGDIR = '/tmp/mnist_data_log'

def placeholder_inputs(batch_size):
	input_placeholder = tf.placeholder(tf.float32, shape=[BATCH_SIZE,784])
	label_placeholder = tf.placeholder(tf.float32,shape=[BATCH_SIZE,10])

	return input_placeholder,label_placeholder

def fill_feed_dict(data_set, images_pl, labels_pl):
	images_feed, labels_feed = data_set.next_batch(BATCH_SIZE)
	feed_dict = {
	  images_pl: images_feed,
	  labels_pl: labels_feed
	}
      	return feed_dict

def loss_function(y_,y):
	return -tf.reduce_sum(y_*tf.log(y))

def full_connect_model(images):
	# hidden1
	with tf.name_scope("hidden_layer1") as scope:
		weights = tf.Variable(tf.truncated_normal([784, 500],
                                              stddev=1.0 / math.sqrt(float(784))),name='weights')
		biases = tf.Variable(tf.zeros([500]),name='biases')

		hidden1 = tf.nn.sigmoid(tf.matmul(images, weights) + biases)
	  # hidden2
	with tf.name_scope("hidden_layer2") as scope:
		weights = tf.Variable(tf.truncated_normal([500, 300],
                                              stddev=1.0 / math.sqrt(float(500))),name='weights')
		biases = tf.Variable(tf.zeros([300]),name='biases')

		hidden2 = tf.nn.sigmoid(tf.matmul(hidden1, weights) + biases)
	  # softmax layer
	with tf.name_scope("softmax_layer") as scope:
		weights = tf.Variable(tf.truncated_normal([300, 10],
                                              stddev=1.0 / math.sqrt(float(300))),name='weights')
		biases = tf.Variable(tf.zeros([10]),name='biases')

		logits = tf.nn.softmax((tf.matmul(hidden2, weights) + biases))
	return logits

def training(loss):
	tf.scalar_summary(loss.op.name, loss)

	optimizer = tf.train.GradientDescentOptimizer(0.01)
	#global_step = tf.Variable(0, name='global_step', trainable=False)
	train_op = optimizer.minimize(loss)

	return train_op

def run_training():
	data_sets = input_data.read_data_sets('MNIST_data',one_hot=True)

	with tf.Graph().as_default():
		images_placeholder, labels_placeholder = placeholder_inputs(BATCH_SIZE)
		logits = full_connect_model(images_placeholder)

		loss = loss_function(labels_placeholder,logits)

		sess = tf.Session()
		train_step = training(loss)

		# get the summary data.
		summary_op = tf.merge_all_summaries()

		sess.run(tf.initialize_all_variables())

		summary_writer = tf.train.SummaryWriter(LOGDIR,
		                                        graph_def=sess.graph_def)
		for step in xrange(10000):
			start_time = time.time()
			feed_dict = fill_feed_dict(data_sets.train,
			                           images_placeholder,
			                           labels_placeholder)
			duration = time.time() - start_time

			_, loss_value = sess.run([train_step, loss],
			                         feed_dict=feed_dict)

			duration = time.time() - start_time

			if step % 100 == 0:
				print('Step %d: loss = %.2f (%.3f sec)' % (step, loss_value, duration))
				# Update the events file.
				summary_str = sess.run(summary_op, feed_dict=feed_dict)
				summary_writer.add_summary(summary_str, step)

if __name__ == '__main__':
	run_training()
